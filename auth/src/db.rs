use crate::error::{BoxError, BoxedError};
use shared::prelude::*;

pub struct Supplier {
    connector: String,
}

impl connpool::Supplier for Supplier {
    type Output = rusqlite::Connection;

    fn supply(&self) -> Self::Output {
        let mut conn = rusqlite::Connection::open(&self.connector).unwrap();
        conn.profile(Some(|q, d| tracing::debug!["[{}] {}", d.as_millis(), q]));
        conn
    }
}

pub(crate) async fn get(conn: String) -> Result<Db, connpool::Error<rusqlite::Error>> {
    let pool = if &conn == ":memory:" {
        connpool::Builder::new()
            .with_pool_size(1)
            .build(Supplier { connector: conn })
    } else {
        connpool::Builder::new()
            .with_pool_size(4)
            .build(Supplier { connector: conn })
    };
    pool.handle(|con| {
        con.execute_batch(
            r#"begin;
create table if not exists authentications (
    type string not null,
    id string not null,
    user_id blob(16) not null,
    primary key ( type, id )
);
commit;
"#,
        )
    })
    .await?;
    Ok(Db(pool))
}

pub(crate) type Pool = connpool::ConnPool<rusqlite::Connection>;

#[derive(Debug, Clone)]
pub(crate) struct Db(Pool);

impl Db {
    pub(crate) async fn upsert(&self, typ: String, id: String) -> Result<uuid::Uuid, BoxError> {
        self.0
            .handle::<_, _, rusqlite::Error>(move |con: &mut rusqlite::Connection| {
                use rusqlite::OptionalExtension;
                let tx = con.transaction()?;
                let nu_id = tx
                    .query_row_named(
                        "select user_id from authentications where type = :type and id = :id",
                        rusqlite::named_params![":type": typ, ":id": id],
                        |row| row.get::<_, uuid::Uuid>(0),
                    )
                    .optional()?;
                if let Some(i) = nu_id {
                    return Ok(i);
                }
                let new_id = uuid::Uuid::new_v4();
                tx.execute_named(
                    "insert into authentications (type,id,user_id) values (:type,:id,:user_id)",
                    rusqlite::named_params![":type": typ, ":id": id, ":user_id": new_id],
                )?;
                tx.commit()?;
                Ok(new_id)
            })
            .await
            .err_boxed()
    }
}
