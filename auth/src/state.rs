use shared::prelude::*;
use tokio::sync::Mutex;

#[derive(PartialEq, Debug, Eq, Clone)]
pub(crate) enum Provider {
    Oauth2 {
        name: String,
    },
    IndieAuth {
        authorization_endpoint: url::Url,
        me: String,
    },
    Foo {
        id: uuid::Uuid,
    },
}

#[derive(PartialEq, Debug, Eq, Clone)]
pub(crate) struct State {
    pub(crate) provider: Provider,
    pub(crate) time: std::time::Instant,
}

pub(crate) struct Pool {
    lru: Mutex<lru::LruCache<String, State>>,
}

impl Pool {
    pub(crate) fn new() -> Self {
        Pool {
            lru: Mutex::new(lru::LruCache::new(42)),
        }
    }

    pub(crate) async fn put(&self, state: State) -> String {
        let id = uuid::Uuid::new_v4().to_string();
        let mut lru = self.lru.lock().await;
        let _ = lru.put(id.clone(), state);
        return id;
    }

    pub(crate) async fn pop(&self, id: &String) -> Option<State> {
        let mut lru = self.lru.lock().await;
        lru.pop(id)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_that_it_works() {
        let pool = Pool::new();
        let state = State {
            provider: Provider::Oauth2 {
                name: "facebook".to_string(),
            },
            time: std::time::Instant::now(),
        };
        let id = pool.put(state.clone()).await;
        let state2 = pool.pop(&id).await;
        assert_eq!(Some(state), state2);
        let state3 = pool.pop(&id).await;
        assert_eq!(None, state3);
    }
}
