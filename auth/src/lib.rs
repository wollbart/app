use shared::prelude::*;
use std::future::Future;
use std::sync::Arc;
use std::task::Poll;

mod db;
mod error;
mod indy;
mod state;

use shared::jwt_factory::{JwtData, JwtFactory};

#[derive(Clone)]
pub(crate) struct Context {
    db: db::Db,
    states: Arc<state::Pool>,
    client: reqwest::Client,
    jwt: Arc<JwtFactory>,
    config: Arc<shared::AuthConfig>,
}

#[derive(Clone)]
pub struct Server {
    db: db::Db,
    states: Arc<state::Pool>,
    client: reqwest::Client,
    jwt: Arc<JwtFactory>,
    pub(crate) config: Arc<shared::AuthConfig>,
}

impl Server {
    pub async fn new(config: &shared::Config) -> Self {
        let jwt = JwtFactory::from_file("key.pem");
        {
            let key = jwt.encode(&JwtData {
                sub: Default::default(),
            });
            jwt.decode(&key).unwrap();
        }
        return Server {
            db: db::get("auth.sqlite".to_string()).await.unwrap(),
            states: Arc::new(state::Pool::new()),
            client: reqwest::Client::new(),
            jwt: Arc::new(jwt),
            config: Arc::new(config.auth.clone()),
        };
    }

    fn context<T>(&self, _req: &hyper::Request<T>) -> Context {
        Context {
            db: self.db.clone(),
            states: self.states.clone(),
            client: self.client.clone(),
            jwt: self.jwt.clone(),
            config: self.config.clone(),
        }
    }
}

impl tower_service::Service<hyper::Request<hyper::Body>> for Server {
    type Response = hyper::Response<hyper::Body>;
    type Error = std::convert::Infallible;
    type Future =
        std::pin::Pin<Box<dyn Future<Output = Result<hyper::Response<hyper::Body>, std::convert::Infallible>> + Send>>;

    fn poll_ready(&mut self, _cx: &mut std::task::Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: hyper::Request<hyper::Body>) -> Self::Future {
        let ctx = self.context(&req);
        let uri = req.uri().clone();
        Box::pin(async move {
            Ok::<_, std::convert::Infallible>(match uri.path() {
                "/auth/start" => indy::start(ctx, req).await,
                "/auth/verify" => indy::verify(ctx, req).await,
                "/auth/mock" => mock(ctx, req).await,
                _ => hyper::Response::builder()
                    .status(404)
                    .body(hyper::Body::from(""))
                    .unwrap(),
            })
        })
    }
}

pub(crate) async fn mock(
    ctx: Context,
    req: hyper::Request<hyper::Body>,
) -> hyper::Response<hyper::Body> {
    #[derive(Debug, serde::Deserialize)]
    struct Params {
        #[serde(default)]
        id: uuid::Uuid,
    };
    #[derive(Debug, serde::Serialize)]
    struct Response<'a> {
        jwt: &'a str,
    }
    let params =
        serde_urlencoded::from_str::<Params>(req.uri().query().unwrap_or_default()).unwrap();
    let jwt = ctx
        .jwt
        .encode(&shared::jwt_factory::JwtData { sub: params.id });
    hyper::Response::builder()
        .status(200)
        .body(hyper::Body::from(
            serde_json::to_string(&Response { jwt: &jwt }).unwrap(),
        ))
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    use mockito::mock;
    use serde_json::json;
    use std::collections::HashMap;
    use tower_service::Service;

    fn match_form(data: &[(&str, &str)]) -> mockito::Matcher {
        mockito::Matcher::AllOf(
            data.iter()
                .map(|(key, value)| mockito::Matcher::UrlEncoded((*key).into(), (*value).into()))
                .collect(),
        )
    }

    async fn server() -> Server {
        Server {
            db: db::get(":memory:".to_string()).await.unwrap(),
            states: Arc::new(state::Pool::new()),
            client: reqwest::Client::new(),
            jwt: Arc::new(JwtFactory::ephemeral()),
            config: Arc::new(HashMap::new()),
        }
    }
    async fn concat<B: hyper::body::HttpBody + std::marker::Unpin>(
        mut body: B,
    ) -> Result<Vec<u8>, B::Error> {
        use hyper::body::Buf;
        // Body
        let mut buf: Vec<u8> = vec![];
        loop {
            let body_fn =
                futures_util::future::poll_fn(|ctx| std::pin::Pin::new(&mut body).poll_data(ctx));
            match body_fn.await {
                Some(Ok(data)) => buf.extend(data.bytes()),
                Some(Err(e)) => return Err(e),
                None => return Ok(buf),
            }
        }
    }
    #[derive(Debug)]
    enum JsonError<E> {
        Buf(E),
        Json(serde_json::Error),
    }
    async fn json<T: serde::de::DeserializeOwned, B: hyper::body::HttpBody + std::marker::Unpin>(
        body: B,
    ) -> Result<T, JsonError<B::Error>> {
        let content = concat(body).await.map_err(JsonError::Buf)?;
        serde_json::from_slice(&content).map_err(JsonError::Json)
    }

    #[tokio::test]
    async fn test_indie_auth_flow_good_path() {
        let _id = mock("GET", "/indie-auth/id")
            .with_status(200)
            .with_body(format![
                r#"<html>
<head>
<link rel="authorization_endpoint" href="{}/indie-auth/auth"></link>
</head>
<body>
<a href="https://github.com/hannesg" rel="me" style="display:none">github.com/hannesg</a>
</body>
</html>"#,
                mockito::server_url()
            ])
            .create();
        let _verify = mock("POST", "/indie-auth/auth")
            .match_body(match_form(&[
                ("code", "1234"),
                ("client_id", indy::CLIENT_ID),
                ("redirect_uri", indy::REDIRECT_URI),
            ]))
            .with_status(200)
            .with_body(
                serde_json::to_string(&json!({
                    "me": format!["{}/indie-auth/id", mockito::server_url()]
                }))
                .unwrap(),
            )
            .create();
        let mut server = server().await;
        let jwt_factory = server.jwt.clone();
        let (head, body) = server
            .call(
                hyper::Request::builder()
                    .method("POST")
                    .uri("/auth/start")
                    .body(hyper::Body::from(
                        serde_json::to_string(&json!({
                            "provider":"indie-auth",
                            "me":format!["{}/indie-auth/id", mockito::server_url()]
                        }))
                        .unwrap(),
                    ))
                    .unwrap(),
            )
            .await
            .unwrap()
            .into_parts();
        let buf = concat(body).await.unwrap();
        assert_eq!(
            head.status,
            200,
            "Bad status: {}\nBody: {}",
            head.status,
            std::str::from_utf8(&*buf).unwrap()
        );
        let content: serde_json::Value = serde_json::from_slice(&*buf).unwrap();
        let redirect: url::Url =
            url::Url::parse(content.get("redirect").unwrap().as_str().unwrap()).unwrap();
        let query = redirect.query_pairs().collect::<HashMap<_, _>>();
        assert_eq!(
            query.get("me").unwrap().to_owned(),
            format!["{}/indie-auth/id", mockito::server_url()]
        );
        assert_eq!(query.get("client_id").unwrap(), indy::CLIENT_ID);
        assert_eq!(query.get("redirect_uri").unwrap(), indy::REDIRECT_URI);
        let state = query.get("state").unwrap().to_owned();
        let (head2, body2) = server
            .call(
                hyper::Request::builder()
                    .method("POST")
                    .uri("/auth/verify")
                    .body(hyper::Body::from(
                        serde_json::to_string(&json!({
                            "state": state,
                            "code": "1234"
                        }))
                        .unwrap(),
                    ))
                    .unwrap(),
            )
            .await
            .unwrap()
            .into_parts();
        assert_eq!(head2.status, 200);
        let jbody = json::<serde_json::Value, _>(body2).await.unwrap();
        jwt_factory.decode(jbody["key"].as_str().unwrap()).unwrap();
        assert_eq!(
            jbody["name"],
            format!["{}/indie-auth/id", mockito::server_url()]
        );
    }

    #[tokio::test]
    async fn test_indie_auth_flow_rejected() {
        let _id = mock("GET", "/indie-auth/id")
            .with_status(200)
            .with_body(format![
                r#"<html>
<head>
<link rel="authorization_endpoint" href="{}/indie-auth/auth"></link>
</head>
<body>
<a href="https://github.com/hannesg" rel="me" style="display:none">github.com/hannesg</a>
</body>
</html>"#,
                mockito::server_url()
            ])
            .create();
        let _verify = mock("POST", "/indie-auth/auth")
            .match_body(match_form(&[
                ("code", "1234"),
                ("client_id", indy::CLIENT_ID),
                ("redirect_uri", indy::REDIRECT_URI),
            ]))
            .with_status(400)
            .with_body(
                serde_json::to_string(&json!({
                    "error": "unauthorized_client",
                }))
                .unwrap(),
            )
            .create();
        let mut server = server().await;
        let (head, body) = server
            .call(
                hyper::Request::builder()
                    .method("POST")
                    .uri("/auth/start")
                    .body(hyper::Body::from(
                        serde_json::to_string(&json!({
                            "provider":"indie-auth",
                            "me":format!["{}/indie-auth/id", mockito::server_url()]
                        }))
                        .unwrap(),
                    ))
                    .unwrap(),
            )
            .await
            .unwrap()
            .into_parts();
        let buf = concat(body).await.unwrap();
        assert_eq!(
            head.status,
            200,
            "Bad status: {}\nBody: {}",
            head.status,
            std::str::from_utf8(&*buf).unwrap()
        );
        let content: serde_json::Value = serde_json::from_slice(&*buf).unwrap();
        let redirect: url::Url =
            url::Url::parse(content.get("redirect").unwrap().as_str().unwrap()).unwrap();
        let query = redirect.query_pairs().collect::<HashMap<_, _>>();
        assert_eq!(
            query.get("me").unwrap().to_owned(),
            format!["{}/indie-auth/id", mockito::server_url()]
        );
        let state = query.get("state").unwrap().to_owned();
        let (head2, body2) = server
            .call(
                hyper::Request::builder()
                    .method("POST")
                    .uri("/auth/verify")
                    .body(hyper::Body::from(
                        serde_json::to_string(&json!({
                            "state": state,
                            "code": "1234"
                        }))
                        .unwrap(),
                    ))
                    .unwrap(),
            )
            .await
            .unwrap()
            .into_parts();
        let buf2 = concat(body2).await.unwrap();
        assert_eq!(
            head2.status,
            500,
            "Bad status: {}\nBody: {}",
            head2.status,
            std::str::from_utf8(&*buf2).unwrap()
        );
    }

    #[tokio::test]
    async fn test_oauth2_facebook_flow_good_path() {
        let _access_token = mock("GET", "/oauth/access_token")
            .match_query(match_form(&[
                ("code", "1234"),
                ("client_id", "wollbart"),
                ("client_secret", "bar"),
                ("redirect_uri", indy::REDIRECT_URI),
            ]))
            .with_status(200)
            .with_body(
                serde_json::to_string(&json!({
                    "access_token": "some-token",
                    "token_type": "code",
                    "expires_in": 86400
                }))
                .unwrap(),
            )
            .create();
        let _me = mock("GET", "/me")
            .match_query(match_form(&[
                ("fields", "id,name"),
                ("access_token", "some-token"),
            ]))
            .with_status(200)
            .with_body(
                serde_json::to_string(&json!({
                    "id": "some-id",
                    "name": "Some Name",
                }))
                .unwrap(),
            )
            .create();
        let mut server = server().await;
        Arc::get_mut(&mut server.config)
            .unwrap()
            .oauth2_providers
            .append(shared::Oauth2Provider {
                name: "facebook".to_string(),
                client_id: "wollbart".to_string(),
                redirect_uri: url::Url::from_str("https://example.com").unwrap(),
                access_token_uri: url::Url::from_str(&format![
                    "{}/oauth/access_token",
                    mockito::server_url()
                ])
                .unwrap(),
                client_secret: "bar".to_string(),
                id_supplier: shared::Oauth2IdSupplier::MeEndpoint {
                    endpoint: url::Url::from_str(&format![
                        "{}/me?fields=id,name",
                        mockito::server_url()
                    ])
                    .unwrap(),
                },
            });
        let jwt_factory = server.jwt.clone();
        let (head, body) = server
            .call(
                hyper::Request::builder()
                    .method("POST")
                    .uri("/auth/start")
                    .body(hyper::Body::from(
                        serde_json::to_string(&json!({
                            "provider":"oauth2",
                            "name": "facebook"
                        }))
                        .unwrap(),
                    ))
                    .unwrap(),
            )
            .await
            .unwrap()
            .into_parts();
        let buf = concat(body).await.unwrap();
        assert_eq!(
            head.status,
            200,
            "Bad status: {}\nBody: {}",
            head.status,
            std::str::from_utf8(&*buf).unwrap()
        );
        let content: serde_json::Value = serde_json::from_slice(&*buf).unwrap();
        let redirect: url::Url =
            url::Url::parse(content.get("redirect").unwrap().as_str().unwrap()).unwrap();
        let query = redirect.query_pairs().collect::<HashMap<_, _>>();
        assert_eq!(query.get("client_id").unwrap(), "wollbart");
        assert_eq!(query.get("redirect_uri").unwrap(), indy::REDIRECT_URI);
        let state = query.get("state").unwrap().to_owned();
        let (head2, body2) = server
            .call(
                hyper::Request::builder()
                    .method("POST")
                    .uri("/auth/verify")
                    .body(hyper::Body::from(
                        serde_json::to_string(&json!({
                            "state": state,
                            "code": "1234"
                        }))
                        .unwrap(),
                    ))
                    .unwrap(),
            )
            .await
            .unwrap()
            .into_parts();
        let buf2 = concat(body2).await.unwrap();
        assert_eq!(
            head2.status,
            200,
            "Bad status: {}\nBody: {}",
            head2.status,
            std::str::from_utf8(&*buf2).unwrap()
        );
        let jbody: serde_json::Value = serde_json::from_slice(&*buf2).unwrap();
        jwt_factory.decode(jbody["key"].as_str().unwrap()).unwrap();
        assert_eq!(jbody["name"], "Some Name");
    }
}
