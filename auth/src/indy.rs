use crate::error::{BoxError, BoxedError};
use crate::state::{self, Provider};
use crate::Context;
use err_derive::Error;
use shared::prelude::*;

pub(crate) const CLIENT_ID: &'static str = "https://wollbart.now.sh";
pub(crate) const REDIRECT_URI: &'static str = "https://wollbart.now.sh/auth";

async fn concat<B: hyper::body::HttpBody + std::marker::Unpin>(
    mut body: B,
) -> Result<Vec<u8>, B::Error> {
    use hyper::body::Buf;
    // Body
    let mut buf: Vec<u8> = vec![];
    loop {
        let body_fn =
            futures_util::future::poll_fn(|ctx| std::pin::Pin::new(&mut body).poll_data(ctx));
        match body_fn.await {
            Some(Ok(data)) => buf.extend(data.bytes()),
            Some(Err(e)) => return Err(e),
            None => return Ok(buf),
        }
    }
}

#[derive(Debug, serde::Deserialize)]
#[serde(tag = "provider")]
enum StartData {
    #[serde(alias = "indie-auth")]
    IndieAuth { me: String },
    #[serde(alias = "oauth2")]
    Oauth2 { name: String },
    #[serde(alias = "foo")]
    Foo { id: uuid::Uuid },
    #[serde(alias = "fail")]
    Fail {
        #[serde(default)]
        error: String,
    },
}

#[derive(Debug, serde::Serialize)]
struct StartResponse {
    redirect: String,
}

#[derive(Debug, serde::Serialize)]
struct RequestBody {
    code: String,
    client_id: String,
    redirect_uri: String,
}

pub(crate) async fn verify(
    ctx: Context,
    req: hyper::Request<hyper::Body>,
) -> hyper::Response<hyper::Body> {
    match verify2(ctx, req).await {
        Err(e) => {
            let status = match e {
                AuthError::InvalidRequest { .. } | AuthError::InvalidJson { .. } => 400,
                _ => 500,
            };
            if status > 499 {
                tracing::error!["{}", e];
            } else {
                tracing::info!["{}", e];
            }
            hyper::Response::builder()
                .status(status)
                .body(hyper::Body::from(format!["{}", e]))
                .unwrap()
        }
        Ok(r) => r,
    }
}

#[derive(Debug, serde::Serialize)]
pub(crate) struct VerifyResponse<'a> {
    key: &'a str,
    name: &'a str,
}

pub(crate) async fn verify2(
    ctx: Context,
    req: hyper::Request<hyper::Body>,
) -> Result<hyper::Response<hyper::Body>, AuthError> {
    #[derive(Debug, serde::Deserialize)]
    struct RequestBody {
        code: String,
        state: String,
    }
    let (_, body) = req.into_parts();
    let data: RequestBody =
        serde_json::from_slice(&concat(body).await.map_err(AuthError::InvalidRequest)?)
            .map_err(AuthError::InvalidJson)?;
    let state = ctx
        .states
        .pop(&data.state)
        .await
        .ok_or(AuthError::MissingAuthState)?;
    match state.provider {
        Provider::IndieAuth {
            authorization_endpoint,
            me,
        } => {
            let span = tracing::info_span!(
                "client.request",
                http.uri = authorization_endpoint.as_str(),
                http.status = tracing::field::Empty
            );
            let _guard = span.enter();
            let response = ctx
                .client
                .post(authorization_endpoint)
                .form(&[
                    ("code", &*data.code),
                    ("client_id", CLIENT_ID),
                    ("redirect_uri", &(*ctx.config).redirect_uri.as_str()),
                ])
                .send()
                .await
                .map_err_boxed(AuthError::IndieAuthValidation)
                .trace_info()?;

            tracing::Span::current().record("http.status", &response.status().as_u16());
            let response = response
                .error_for_status()
                .map_err_boxed(AuthError::IndieAuthValidation)
                .trace_info()?;
            #[derive(serde::Deserialize)]
            struct Me {
                me: String,
            }
            let result = response
                .json::<Me>()
                .await
                .map_err_boxed(AuthError::IndieAuthValidation)
                .trace_info()?;
            if result.me != me {
                Err(AuthError::IndieAuthMismatchingMe {
                    expected: me,
                    actual: result.me,
                })
            } else {
                let id: uuid::Uuid = ctx
                    .db
                    .upsert("indie-auth".to_string(), result.me)
                    .await
                    .map_err_boxed(AuthError::Internal)?;

                let jwt = ctx.jwt.encode(&crate::JwtData { sub: id });
                Ok(hyper::Response::builder()
                    .status(200)
                    .body(hyper::Body::from(
                        serde_json::to_string(&VerifyResponse {
                            key: &jwt,
                            name: &me,
                        })
                        .unwrap(),
                    ))
                    .unwrap())
            }
        }
        Provider::Oauth2 { name } => {
            #[derive(serde::Deserialize)]
            struct OAuth2Response {
                access_token: String,
                id_token: Option<String>,
                expires_in: u64,
            }
            let pro = (*ctx.config)
                .oauth2_providers
                .iter()
                .filter(|p| p.name == name)
                .next()
                .ok_or_else(|| AuthError::OAuth2UnknownProvider {
                    provider: name.clone(),
                })?;
            let response = ctx
                .client
                .get(pro.access_token_uri.as_str())
                .query(&[
                    ("code", &*data.code),
                    ("client_id", &pro.client_id),
                    ("client_secret", &pro.client_secret),
                    ("redirect_uri", &(*ctx.config).redirect_uri.as_str()),
                ])
                .send()
                .await
                .map_err_boxed(AuthError::OAuth2Validation)?
                .error_for_status()
                .map_err_boxed(AuthError::OAuth2Validation)?
                .json::<OAuth2Response>()
                .await
                .map_err_boxed(AuthError::OAuth2Validation)?;
            match &pro.id_supplier {
                shared::Oauth2IdSupplier::MeEndpoint { endpoint } => {
                    #[derive(serde::Deserialize)]
                    struct OAuth2MeResponse {
                        id: String,
                        name: Option<String>,
                    }
                    let me_response = ctx
                        .client
                        .get(endpoint.as_str())
                        .query(&[("access_token", &response.access_token)])
                        .send()
                        .await
                        .map_err_boxed(AuthError::OAuth2Validation)?
                        .error_for_status()
                        .map_err_boxed(AuthError::OAuth2Validation)?
                        .json::<OAuth2MeResponse>()
                        .await
                        .map_err_boxed(AuthError::OAuth2Validation)?;
                    let id: uuid::Uuid = ctx
                        .db
                        .upsert(format!["oauth2/{}", &pro.name], me_response.id.clone())
                        .await
                        .map_err_boxed(AuthError::Internal)?;

                    let jwt = ctx.jwt.encode(&crate::JwtData { sub: id });
                    Ok(hyper::Response::builder()
                        .status(200)
                        .body(hyper::Body::from(
                            serde_json::to_string(&VerifyResponse {
                                key: &jwt,
                                name: &*me_response.name.as_ref().unwrap_or(&me_response.id),
                            })
                            .unwrap(),
                        ))
                        .unwrap())
                }
            }
        }
        Provider::Foo { id } => {
            let jwt = ctx.jwt.encode(&crate::JwtData { sub: id });
            Ok(hyper::Response::builder()
                .status(200)
                .body(hyper::Body::from(
                    serde_json::to_string(&VerifyResponse {
                        key: &jwt,
                        name: "Foo",
                    })
                    .unwrap(),
                ))
                .unwrap())
        }
    }
}

#[derive(Debug, Error)]
pub(crate) enum AuthError {
    #[error(display = "A100: Invalid request: {}", _0)]
    InvalidRequest(#[error(source, no_from)] hyper::Error),
    #[error(display = "A101: Invalid json: {}", _0)]
    InvalidJson(#[error(source)] serde_json::Error),
    #[error(display = "A110: Invalid IndieAuth me: {}", _0)]
    IndieAuthInvalidMe(#[error(source, no_from)] BoxError),
    #[error(display = "A111: Invalid IndieAuth me: Bad protocol {:?}", protocol)]
    IndieAuthInvalidMeBadProtocol { protocol: String },
    #[error(display = "A112: Invalid IndieAuth me: Fragment is present")]
    IndieAuthInvalidMeFragmentIsPresent,
    #[error(display = "A121: Invalid IndieAuth authorization endpoint")]
    IndieAuthMissingAuthorizationEndpoint,
    #[error(display = "A122: Failed IndieAuth discovery: {}", _0)]
    IndieAuthFailedDiscovery(#[error(source, no_from)] BoxError),
    #[error(display = "A150: Missing auth state")]
    MissingAuthState,
    #[error(display = "A151: Failed validating IndieAuth code: {}", _0)]
    IndieAuthValidation(#[error(source, no_from)] BoxError),
    #[error(
        display = "A152: Mismatching IndieAuth identities: {} vs. {}",
        expected,
        actual
    )]
    IndieAuthMismatchingMe { expected: String, actual: String },
    #[error(display = "A123: Unkwown oauth2 provider {:?}", provider)]
    OAuth2UnknownProvider { provider: String },
    #[error(display = "A153: Failed validating OAuth2 code: {}", _0)]
    OAuth2Validation(#[error(source, no_from)] BoxError),
    #[error(display = "A000: Internal: {}", _0)]
    Internal(#[error(source)] BoxError),
}

pub(crate) async fn start(
    ctx: Context,
    req: hyper::Request<hyper::Body>,
) -> hyper::Response<hyper::Body> {
    match start2(ctx, req).await {
        Ok(res) => res,
        Err(e) => {
            let status = match e {
                AuthError::IndieAuthInvalidMe(..) => 400,
                AuthError::IndieAuthMismatchingMe { .. } => 400,
                AuthError::IndieAuthFailedDiscovery { .. } => {
                    tracing::error!["{}", e];
                    400
                }
                _ => {
                    tracing::error!["{}", e];
                    500
                }
            };
            hyper::Response::builder()
                .status(status)
                .body(hyper::Body::from(format!["{}", e]))
                .unwrap()
        }
    }
}

pub(crate) fn indie_auth_normalize_url(s: &str) -> Result<url::Url, AuthError> {
    match s.find(":") {
        Some(p) => {
            let proto = &s[..p];
            if proto != "http" && proto != "https" {
                return Err(AuthError::IndieAuthInvalidMeBadProtocol {
                    protocol: proto.to_owned(),
                });
            }
            let u: url::Url = url::Url::parse(s).map_err_boxed(AuthError::IndieAuthInvalidMe)?;
            if u.fragment().is_some() {
                return Err(AuthError::IndieAuthInvalidMeFragmentIsPresent);
            }
            Ok(u)
        }
        None => indie_auth_normalize_url(&format!["https://{}", s]),
    }
}

pub(crate) async fn start2(
    ctx: Context,
    req: hyper::Request<hyper::Body>,
) -> Result<hyper::Response<hyper::Body>, AuthError> {
    let (_head, body) = req.into_parts();
    let data: StartData =
        serde_json::from_slice(&concat(body).await.map_err(AuthError::InvalidRequest)?)
            .map_err(AuthError::InvalidJson)?;
    match data {
        StartData::IndieAuth { me } => {
            let uri = indie_auth_normalize_url(&*me)?;
            let response = ctx
                .client
                .get(uri.clone())
                .send()
                .await
                .map_err_boxed(AuthError::IndieAuthFailedDiscovery)?;
            let authorization_endpoint = authorization_endpoint(response).await?;
            let state = state::State {
                provider: state::Provider::IndieAuth {
                    authorization_endpoint: authorization_endpoint.clone(),
                    me: uri.as_str().to_string(),
                },
                time: std::time::Instant::now(),
            };
            let id = ctx.states.put(state).await;
            let mut redirect_uri: url::Url = authorization_endpoint;
            redirect_uri
                .query_pairs_mut()
                .append_pair("me", uri.as_str())
                .append_pair("client_id", &*CLIENT_ID)
                .append_pair("redirect_uri", &(*ctx.config).redirect_uri.as_str())
                .append_pair("state", &*id);
            Ok(hyper::Response::builder()
                .status(200)
                .body(hyper::Body::from(
                    serde_json::to_string(&StartResponse {
                        redirect: redirect_uri.to_string(),
                    })
                    .unwrap(),
                ))
                .unwrap())
        }
        StartData::Oauth2 { name } => {
            let pro = (*ctx.config)
                .oauth2_providers
                .iter()
                .filter(|p| p.name == name)
                .next()
                .ok_or_else(|| AuthError::OAuth2UnknownProvider {
                    provider: name.clone(),
                })?;
            let state = state::State {
                provider: state::Provider::Oauth2 { name },
                time: std::time::Instant::now(),
            };
            let id = ctx.states.put(state).await;
            let mut redirect_uri: url::Url = pro.redirect_uri.clone();
            redirect_uri
                .query_pairs_mut()
                .append_pair("client_id", &*pro.client_id)
                .append_pair("redirect_uri", &(*ctx.config).redirect_uri.as_str())
                .append_pair("state", &*id);
            Ok(hyper::Response::builder()
                .status(200)
                .body(hyper::Body::from(
                    serde_json::to_string(&StartResponse {
                        redirect: redirect_uri.to_string(),
                    })
                    .unwrap(),
                ))
                .unwrap())
        }
        StartData::Foo { id } => {
            let state = state::State {
                provider: state::Provider::Foo { id },
                time: std::time::Instant::now(),
            };
            let id = ctx.states.put(state).await;
            let mut redirect_uri: url::Url = (*ctx.config).redirect_uri.clone();
            redirect_uri
                .query_pairs_mut()
                .append_pair("state", &*id)
                .append_pair("code", "foo");
            Ok(hyper::Response::builder()
                .status(200)
                .body(hyper::Body::from(
                    serde_json::to_string(&StartResponse {
                        redirect: redirect_uri.to_string(),
                    })
                    .unwrap(),
                ))
                .unwrap())
        }
        StartData::Fail { error } => {
            let mut redirect_uri: url::Url = (*ctx.config).redirect_uri.clone();
            redirect_uri.query_pairs_mut().append_pair("error", &*error);
            Ok(hyper::Response::builder()
                .status(200)
                .body(hyper::Body::from(
                    serde_json::to_string(&StartResponse {
                        redirect: redirect_uri.to_string(),
                    })
                    .unwrap(),
                ))
                .unwrap())
        }
    }
}

async fn authorization_endpoint(response: reqwest::Response) -> Result<url::Url, AuthError> {
    let data = response
        .text()
        .await
        .map_err_boxed(AuthError::IndieAuthFailedDiscovery)?;
    let selector = scraper::Selector::parse("head > link[rel=authorization_endpoint]").unwrap();
    let doc = scraper::Html::parse_document(&*data);
    let link = doc
        .select(&selector)
        .flat_map(|it| it.value().attr("href"))
        .next()
        .ok_or(AuthError::IndieAuthMissingAuthorizationEndpoint)?;
    let authorization_endpoint: url::Url =
        url::Url::parse(&*link).map_err_boxed(AuthError::IndieAuthFailedDiscovery)?;
    return Ok(authorization_endpoint);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_normalizes_urls_without_path() {
        assert_eq!(
            indie_auth_normalize_url("http://foo.org").unwrap().as_str(),
            "http://foo.org/"
        )
    }

    #[test]
    fn it_normalizes_urls_without_path_and_query() {
        assert_eq!(
            indie_auth_normalize_url("http://foo.org?bla=blub")
                .unwrap()
                .as_str(),
            "http://foo.org/?bla=blub"
        )
    }

    #[test]
    fn it_normalizes_urls_without_schema() {
        assert_eq!(
            indie_auth_normalize_url("foo.org").unwrap().as_str(),
            "https://foo.org/"
        )
    }

    #[test]
    fn it_rejects_urls_with_bad_schema() {
        assert_eq!(
            indie_auth_normalize_url("mailto:foo.org")
                .unwrap_err()
                .to_string(),
            "A111: Invalid IndieAuth me: Bad protocol \"mailto\""
        )
    }

    #[test]
    fn it_rejects_urls_with_fragment() {
        assert_eq!(
            indie_auth_normalize_url("https://foo.org/#frag")
                .unwrap_err()
                .to_string(),
            "A112: Invalid IndieAuth me: Fragment is present"
        )
    }
}
