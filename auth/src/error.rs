use err_derive::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error(display = "Rusqlite: {}", _0)]
    Rusqlite(#[error(source)] rusqlite::Error),
    #[error(display = "Serde: {}", _0)]
    Serde(#[error(source)] serde_json::Error),
}

pub(crate) struct BoxError(Box<dyn std::error::Error + Send + Sync>);

impl std::fmt::Debug for BoxError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}

impl std::fmt::Display for BoxError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.0, f)
    }
}

impl std::error::Error for BoxError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        self.0.source()
    }
}

pub(crate) trait BoxedError {
    type Value;
    fn map_err_boxed<E, F: FnOnce(BoxError) -> E>(self, f: F) -> Result<Self::Value, E>;
    fn err_boxed(self) -> Result<Self::Value, BoxError>;
}

impl<T, E2: std::error::Error + Send + Sync + 'static> BoxedError for Result<T, E2> {
    type Value = T;
    fn map_err_boxed<E, F: FnOnce(BoxError) -> E>(self, f: F) -> Result<Self::Value, E> {
        self.map_err(|e| f(BoxError(Box::new(e))))
    }
    fn err_boxed(self) -> Result<Self::Value, BoxError> {
        self.map_err(|e| BoxError(Box::new(e)))
    }
}
