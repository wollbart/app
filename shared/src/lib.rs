pub mod jwt_factory;
pub mod trace_error {
    pub trait TraceError {
        fn trace_info(self) -> Self;
    }
    impl<T, E: std::fmt::Display> TraceError for Result<T, E> {
        fn trace_info(self) -> Self {
            match &self {
                Err(e) => {
                    ::tracing::info![%e];
                }
                _ => {}
            };
            return self;
        }
    }
}
pub mod prelude {
    pub use crate::trace_error::TraceError;
    pub use ::tracing;
    pub use ::tracing_futures;
    pub use ::tracing_subscriber;
    pub use ::url;
    pub use ::uuid;
}
/*
pub mod jwt {
    fn find_bearer(header: &http::header::HeaderMap<http::header::HeaderValue>) -> Option<&str> {
        header
            .get_all("Authorization")
            .iter()
            .flat_map(|hdr| {
                hdr.to_str().ok().and_then(|s| {
                    if s.starts_with("Bearer ") {
                        Some(&s["Bearer ".len()..])
                    } else {
                        None
                    }
                })
            })
            .next()
    }
}*/

pub(crate) const CLIENT_ID: &'static str = "https://wollbart.now.sh";
pub(crate) const REDIRECT_URI: &'static str = "https://wollbart.now.sh/auth";

#[derive(Debug, Clone, serde::Deserialize)]
#[serde(tag = "type")]
pub enum Oauth2IdSupplier {
    #[serde(alias = "me")]
    MeEndpoint { endpoint: url::Url },
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct Oauth2Provider {
    pub name: String,
    pub client_id: String,
    pub redirect_uri: url::Url,
    pub access_token_uri: url::Url,
    pub client_secret: String,
    pub id_supplier: Oauth2IdSupplier,
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct IndieAuthConfig {
    #[serde(default = "IndieAuthConfig::default_client_id")]
    pub client_id: String,
}
impl IndieAuthConfig {
    fn default_client_id() -> String {
        CLIENT_ID.to_owned()
    }
}
impl Default for IndieAuthConfig {
    fn default() -> Self {
        IndieAuthConfig {
            client_id: IndieAuthConfig::default_client_id(),
        }
    }
}

impl AuthConfig {
    fn default_redirect_uri() -> url::Url {
        url::Url::parse(REDIRECT_URI).unwrap()
    }
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct AuthConfig {
    #[serde(default = "AuthConfig::default_redirect_uri")]
    pub redirect_uri: url::Url,
    #[serde(default)]
    pub oauth2_providers: Vec<Oauth2Provider>,
    #[serde(default)]
    pub indie_auth: IndieAuthConfig,
    #[serde(default)]
    pub foo: bool,
}
impl Default for AuthConfig {
    fn default() -> Self {
        AuthConfig {
            redirect_uri: AuthConfig::default_redirect_uri(),
            oauth2_providers: Default::default(),
            indie_auth: Default::default(),
            foo: false,
        }
    }
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct JwtConfig {
    key: std::path::PathBuf,
}

impl JwtConfig {
    fn default_key() -> std::path::PathBuf {
        std::path::PathBuf::from("key.pem")
    }

    pub fn factory(&self) -> jwt_factory::JwtFactory {
        jwt_factory::JwtFactory::from_file(&self.key)
    }
}

impl Default for JwtConfig {
    fn default() -> Self {
        JwtConfig {
            key: JwtConfig::default_key(),
        }
    }
}

#[derive(Debug, Default, Clone, serde::Deserialize)]
pub struct Config {
    #[serde(default)]
    pub auth: AuthConfig,
    #[serde(default)]
    pub jwt: JwtConfig,
}

impl Config {
    pub fn from_file() -> Self {
        use std::io::Read;
        match std::fs::File::open("config.json") {
            Ok(mut file) => {
                tracing::debug![file = "config.json", "config.read"];
                let mut buf = Vec::new();
                file.read_to_end(&mut buf).unwrap();
                serde_json::from_slice::<Config>(&*buf).unwrap()
            }
            Err(err) if err.kind() == std::io::ErrorKind::NotFound => {
                tracing::info![file = "config.json", "config.missing"];
                Default::default()
            }
            Err(err) => panic!["Error reading config: {}", err],
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_parses_an_empty_config() {
        let config = serde_json::from_str::<Config>("{}").unwrap();
        assert_eq!(config.auth.redirect_uri.as_str(), REDIRECT_URI);
        assert_eq!(config.auth.indie_auth.client_id, CLIENT_ID);
        assert_eq!(config.jwt.key.to_str(), Some("key.pem"));
    }
}
