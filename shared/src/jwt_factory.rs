use err_derive::Error;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct JwtData {
    pub sub: uuid::Uuid,
}

#[derive(Debug, Clone)]
pub struct JwtFactory {
    key: Vec<u8>,
    pub public_key: Vec<u8>,
}

struct PublicKey {
    data: Vec<u8>,
}

#[derive(Debug, Error)]
pub enum JwtError {
    #[error(display = "J100: Invalid token: {}", _0)]
    ValidationFailed(#[error(source)] jsonwebtoken::errors::Error),
}

impl simple_asn1::ToASN1 for PublicKey {
    type Error = simple_asn1::ASN1EncodeErr;
    fn to_asn1_class(
        &self,
        _c: simple_asn1::ASN1Class,
    ) -> Result<Vec<simple_asn1::ASN1Block>, Self::Error> {
        use simple_asn1::*;
        return Ok(vec![
            simple_asn1::ASN1Block::ObjectIdentifier(0, simple_asn1::oid!(1, 2, 840, 10_045, 2, 1)),
            simple_asn1::ASN1Block::OctetString(0, self.data.clone()),
        ]);
    }
}

#[derive(Debug, serde::Serialize)]
struct JwtDataWrapper<'a> {
    #[serde(flatten)]
    data: &'a JwtData,
    exp: u64,
}

impl JwtFactory {
    pub fn from_file<T: AsRef<std::path::Path>>(path: T) -> Self {
        use std::io::Read;
        let key = match std::fs::File::open(path) {
            Ok(mut file) => {
                let mut buf = Vec::new();
                file.read_to_end(&mut buf).unwrap();
                let data = pem::parse(buf).unwrap();
                if data.tag != "PRIVATE KEY" {
                    panic!["Invalid pem tag: {}", &data.tag];
                }
                data.contents
            }
            Err(err) if err.kind() == std::io::ErrorKind::NotFound => {
                let rng = ring::rand::SystemRandom::new();
                let pkcs8_bytes = ring::signature::EcdsaKeyPair::generate_pkcs8(
                    &ring::signature::ECDSA_P384_SHA384_FIXED_SIGNING,
                    &rng,
                )
                .unwrap();
                pkcs8_bytes.as_ref().to_owned()
            }
            Err(err) => panic![err],
        };
        return Self::from_private(key);
    }

    pub fn ephemeral() -> Self {
        let rng = ring::rand::SystemRandom::new();
        let pkcs8_bytes = ring::signature::EcdsaKeyPair::generate_pkcs8(
            &ring::signature::ECDSA_P384_SHA384_FIXED_SIGNING,
            &rng,
        )
        .unwrap();
        return Self::from_private(pkcs8_bytes.as_ref().to_owned());
    }

    pub(crate) fn from_private(key: Vec<u8>) -> Self {
        use ring::signature::KeyPair;
        let public_key = ring::signature::EcdsaKeyPair::from_pkcs8(
            &ring::signature::ECDSA_P384_SHA384_FIXED_SIGNING,
            &*key,
        )
        .unwrap()
        .public_key()
        .as_ref()
        .to_owned();
        let p = simple_asn1::der_encode(&PublicKey { data: public_key }).unwrap();
        Self {
            key: pem::encode(&pem::Pem {
                tag: String::from("PRIVATE KEY"),
                contents: key,
            })
            .into(),
            public_key: pem::encode(&pem::Pem {
                tag: String::from("PUBLIC KEY"),
                contents: p,
            })
            .into(),
        }
    }

    pub fn public(&self) -> <ring::signature::EcdsaKeyPair as ring::signature::KeyPair>::PublicKey {
        use ring::signature::KeyPair;
        let data = pem::parse(&*self.key).unwrap().contents;
        return *ring::signature::EcdsaKeyPair::from_pkcs8(
            &ring::signature::ECDSA_P384_SHA384_FIXED_SIGNING,
            &*data,
        )
        .unwrap()
        .public_key();
    }

    pub fn encode(&self, data: &JwtData) -> String {
        jsonwebtoken::encode(
            &jsonwebtoken::Header::new(jsonwebtoken::Algorithm::ES384),
            &JwtDataWrapper {
                data: &data,
                exp: std::time::SystemTime::now()
                    .duration_since(std::time::UNIX_EPOCH)
                    .unwrap()
                    .as_secs()
                    + 86400,
            },
            &*self.key,
        )
        .unwrap()
    }

    pub fn decode(&self, token: &str) -> Result<JwtData, JwtError> {
        Ok(jsonwebtoken::decode::<JwtData>(
            token,
            &*self.public_key,
            &jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::ES384),
        )?
        .claims)
    }
}
