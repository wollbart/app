use std::marker::PhantomData;

#[derive(Clone,Debug)]
pub(crate) enum OneOrTwo<T> {
    One([T; 1]),
    Two([T; 2])
}

impl<T> AsRef<[T]> for OneOrTwo<T> {
    fn as_ref(&self) -> &[T] {
        match self {
            OneOrTwo::One(a) => a.as_ref(),
            OneOrTwo::Two(a) => a.as_ref()
        }
    }
}

impl<T> OneOrTwo<T> {
    pub(crate) fn one(t: T) -> Self {
        OneOrTwo::One([t])
    }
    pub(crate) fn two(t: T, u: T) -> Self {
        OneOrTwo::Two([t, u])
    }
}

impl<T> serde::Serialize for OneOrTwo<T>
    where T: serde::Serialize
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        use serde::ser::SerializeSeq;
        let r = self.as_ref();
        let mut s = serializer.serialize_seq(Some(r.len()))?;
        for t in r.iter() {
            s.serialize_element(t)?;
        }
        s.end()
    }
}

impl<'de, T> serde::Deserialize<'de> for OneOrTwo<T>
    where T: serde::Deserialize<'de> {

    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de> {
        struct V<U>(PhantomData<U>);
        impl<'de, U> serde::de::Visitor<'de> for V<U> 
            where U: serde::Deserialize<'de>
            {
            type Value = OneOrTwo<U>;

            fn expecting(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
                f.write_str("one or two values")
            }

            fn visit_seq<S>(self, mut seq: S) -> Result<OneOrTwo<U>, S::Error>
            where
                S: serde::de::SeqAccess<'de>,
            {
                // Start with max equal to the first value in the seq.
                let first = seq.next_element()?.ok_or_else(||
                    // Cannot take the maximum of an empty seq.
                    serde::de::Error::custom("no values in seq")
                )?;
                if let Some(second) = seq.next_element()? {
                    Ok(OneOrTwo::two(first, second))
                } else {
                    Ok(OneOrTwo::one(first))
                }
            }
        }

        deserializer.deserialize_seq(V(PhantomData))
    }
}


