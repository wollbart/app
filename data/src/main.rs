pub async fn server() -> () {
    let server = data::Server::new().await;
    let builder = if let Ok(Some(l)) = listenfd::ListenFd::from_env().take_tcp_listener(0) {
        hyper::Server::from_tcp(l).unwrap()
    } else {
        hyper::Server::bind(&([127u8, 0, 0, 1], 12345u16).into())
    };
    builder
        .serve(hyper::service::make_service_fn(
            move |_: &hyper::server::conn::AddrStream| {
                let s = server.clone();
                async { Ok::<_, !>(s) }
            },
        ))
        .await
        .unwrap()
}

#[tokio::main]
async fn main() {
    server().await;
}
