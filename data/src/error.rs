use err_derive::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error(display = "Rusqlite: {}", _0)]
    Rusqlite(#[error(source)] rusqlite::Error),
    #[error(display = "Serde: {}", _0)]
    Serde(#[error(source)] serde_json::Error),
    #[error(display = "Unauthenticated")]
    Unauthenticated,
}
