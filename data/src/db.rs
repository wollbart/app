use shared::prelude::*;

pub struct Supplier {
    connector: String,
}

impl connpool::Supplier for Supplier {
    type Output = rusqlite::Connection;

    fn supply(&self) -> Self::Output {
        let mut conn = rusqlite::Connection::open(&self.connector).unwrap();

        conn.profile(Some(|q, d| {
            tracing::info![duration = d.as_millis() as u64, query = q, "query"]
        }));
        conn
    }
}

pub async fn get(conn: String) -> Result<Pool, connpool::Error<rusqlite::Error>> {
    let pool = if &conn == ":memory:" {
        connpool::Builder::new()
            .with_pool_size(1)
            .build(Supplier { connector: conn })
    } else {
        connpool::Builder::new()
            .with_pool_size(4)
            .build(Supplier { connector: conn })
    };
    pool.handle(|con| {
        con.execute_batch(
            r#"begin;
create table if not exists user (
    id blob(16) not null primary key
);
create table if not exists project (
    id blob(16) not null primary key,
    user blob(16),
    data text not null
);
create table if not exists progress (
    id blob(16) not null,
    timestamp integer not null,
    value integer
);
commit;
"#,
        )
    })
    .await?;
    Ok(pool)
}

pub type Pool = connpool::ConnPool<rusqlite::Connection>;
