use std::future::Future;
use std::sync::Arc;
use std::task::Poll;
use uuid::Uuid;

mod data;
mod db;
mod error;
mod one_or_two;
mod schema;

#[derive(Clone)]
struct Context {
    user: Option<Uuid>,
    db: db::Pool,
}

impl Context {
    fn auth(&self) -> Result<Uuid, error::Error> {
        self.user.ok_or(error::Error::Unauthenticated)
    }
}

#[derive(Clone)]
pub struct Server {
    db: db::Pool,
    jwt: Arc<shared::jwt_factory::JwtFactory>,
}

impl Server {
    pub async fn new(config: &shared::Config) -> Self {
        Server {
            db: db::get("db.sqlite".to_string()).await.unwrap(),
            jwt: Arc::new(config.jwt.factory()),
        }
    }
    fn context<T>(&self, req: &hyper::Request<T>) -> Result<Context, hyper::Response<hyper::Body>> {
        let bearer = find_bearer(req.headers());
        let user = bearer
            .map(|s| self.jwt.decode(s))
            .transpose()
            .map_err(|err| {
                hyper::Response::builder()
                    .status(403)
                    .body(hyper::Body::from(""))
                    .unwrap()
            })?;
        Ok(Context {
            user: user.map(|u| u.sub),
            db: self.db.clone(),
        })
    }
}

fn find_bearer(header: &http::header::HeaderMap<http::header::HeaderValue>) -> Option<&str> {
    header
        .get_all("Authorization")
        .iter()
        .flat_map(|hdr| {
            hdr.to_str().ok().and_then(|s| {
                if s.starts_with("Bearer ") {
                    Some(&s["Bearer ".len()..])
                } else {
                    None
                }
            })
        })
        .next()
}

impl tower_service::Service<hyper::Request<hyper::Body>> for Server {
    type Response = hyper::Response<hyper::Body>;
    type Error = std::convert::Infallible;
    type Future =
        std::pin::Pin<Box<dyn Future<Output = Result<hyper::Response<hyper::Body>, Self::Error>> + Send>>;

    fn poll_ready(&mut self, _cx: &mut std::task::Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: hyper::Request<hyper::Body>) -> Self::Future {
        match self.context(&req) {
            Ok(ctx) => Box::pin(async move { Ok::<_, Self::Error>(data::serve(ctx, req).await) }),
            Err(res) => Box::pin(futures::future::ready(Ok(res))),
        }
    }
}
