earl_macro::graphql_schema!(r#"

scalar Color
scalar Datetime

type Progress {
    timestamp: Int!
    value: Int
}

type PageInfo {
    endCursor: String,
    hasNextPage: Boolean!
}

type DailyProgress {
    dailyProgress: Int!
    from: Int!
}

type Plan {
    dueBy: Datetime!,
    startedAt: Datetime!,
    dailyProgress: [DailyProgress!]!
}

type Project {
    id: ID!
    name: String!
    lastUpdate: Int
    pattern: Pattern!
    plan: Plan
    progress: ProgressListingConnection!
}

type ProgressListingConnection {
    nodes: [Progress!]!
    pageInfo: PageInfo!
}

type ProjectListingConnection {
    nodes: [Project!]!
    pageInfo: PageInfo!
    totalCount: Int!
}

type Query {
    projects(
        after: String
    ): ProjectListingConnection!
    project(
        id: ID!
    ): Project
}

type Pattern {
    width: Int!,
    height: Int!
    data: [Int!]!
    colors: [Color!]!
}

input PatternInput {
    width: Int!,
    height: Int!
    data: [Int!]!
    colors: [Color!]!
}

input DailyProgressInput {
    from: Int!,
    dailyProgress: Int!
}

input PlanInput {
    dueBy: Datetime!
    dailyProgress: [DailyProgressInput!]!
    startedAt: Datetime!
}

input ProjectInput {
    id: ID!
    pattern: PatternInput!
    name: String!
    plan: PlanInput
}

input UpdateProjectInput {
    timestamp: Int!
    value: Int
}

type Mutation {
    createProject(input: ProjectInput!): Project
    updateProject(
        id: ID!,
        lastUpdate: Int,
        input: [UpdateProjectInput!]!
    ): Int!
}

schema {
    query: Query
    mutation: Mutation
}
"#
);

#[derive(Debug,Clone,PartialEq,Eq,Copy)]
pub struct Color {
    pub rgb: [u8; 3]
}

impl Color {
    fn from_input(b: &str) -> Result<Self, earl::Error> {
        if b.len() != 9 {
            Err(earl::Error::invalid_argument(format!["Invalid length: {}", b.len()]))
        } else if &b[0..1] != "#" {
            Err(earl::Error::invalid_argument(format!["Doesn't start with '#'"]))
        } else if b[1..].chars().all(|c| c.is_ascii_hexdigit() ) {
            Ok(Color{
                rgb: [
                    u8::from_str_radix(&b[1..=2], 16).unwrap(),
                    u8::from_str_radix(&b[3..=4], 16).unwrap(),
                    u8::from_str_radix(&b[5..=6], 16).unwrap()
                ]
            })
        } else {
            Err(earl::Error::new(earl::ErrorKind::Query))
        }
    }
}

impl earl::Scalar for Color {
    fn read_value(value: &earl::graphql_parser::query::Value<'static, String>) -> Result<Self, earl::Error> {
        match value {
            earl::graphql_parser::query::Value::String(s) => {
                Color::from_input(&*s)
            },
            _ => Err(earl::Error::new(earl::ErrorKind::Query))
        }
    }

    fn write_value(&self, writer: earl::ScalarWriter) -> Result<(), earl::Error> {
        use std::io::Write;
        let mut buf : [u8; 9] = [0; 9];
        write!(buf.as_mut(), "#{:02x}{:02x}{:02x}ff", &self.rgb[0], &self.rgb[1], &self.rgb[2]).unwrap();
        writer.write_str(std::str::from_utf8(buf.as_ref()).unwrap())
    }
}

impl serde::Serialize for Color {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer
    {
        use std::io::Write;
        let mut buf : [u8; 9] = [0; 9];
        write!(buf.as_mut(), "#{:02x}{:02x}{:02x}ff", &self.rgb[0], &self.rgb[1], &self.rgb[2]).unwrap();
        serializer.serialize_str(std::str::from_utf8(buf.as_ref()).unwrap())
    }
}

impl<'de> serde::Deserialize<'de> for Color  {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de> {
        struct V();
        impl<'de> serde::de::Visitor<'de> for V {
            type Value = Color;

            fn expecting(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
                f.write_str("a color")
            }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Color::from_input(s).map_err(serde::de::Error::custom)
            }
        }
        deserializer.deserialize_str(V())
    }
}

#[derive(Debug,Clone,PartialEq,Eq,Copy,serde::Deserialize,serde::Serialize)]
#[serde(from="chrono::DateTime<chrono::FixedOffset>", into="chrono::DateTime<chrono::FixedOffset>")]
pub struct Datetime {
    pub datetime: chrono::DateTime<chrono::FixedOffset>
}

impl earl::Scalar for Datetime {
    fn read_value(value: &earl::graphql_parser::query::Value<'static,String>) -> Result<Self, earl::Error> {
        match value {
            earl::graphql_parser::query::Value::String(s) => {
                chrono::DateTime::parse_from_rfc3339(&*s).map_err(|e|
                    earl::Error::invalid_argument("Invalid date".to_string())
                        .with_source(Box::new(e))
                        .with_extension(earl::extensions::VariableValue(value.clone()))
                ).map(|d|
                      Datetime{ datetime: d }
                      )
            },
            _ => Err(earl::Error::new(earl::ErrorKind::Query))
        }
    }

    fn write_value(&self, writer: earl::ScalarWriter) -> Result<(), earl::Error> {
        writer.write_str(&self.datetime.to_rfc3339())
    }
}

impl From<chrono::DateTime<chrono::FixedOffset>> for Datetime {
    fn from(datetime: chrono::DateTime<chrono::FixedOffset>) -> Self {
        Datetime{ datetime }
    }
}

impl Into<chrono::DateTime<chrono::FixedOffset>> for Datetime {
    fn into(self) -> chrono::DateTime<chrono::FixedOffset> {
        self.datetime
    }
}
