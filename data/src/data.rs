use crate::error;
use crate::one_or_two::OneOrTwo;
use crate::schema;
use crate::schema::Color;
use crate::Context;

use rusqlite::named_params;
use rusqlite::OptionalExtension;
use uuid::Uuid;

pub(crate) async fn serve(
    ctx: Context,
    req: hyper::Request<hyper::Body>,
) -> hyper::Response<hyper::Body> {
    earl::hyper::call::<schema::Schema, _>(ctx, req).await
}

#[derive(Debug, Clone)]
struct PageInfo {
    end_cursor: Option<String>,
}

impl schema::page_info::Instance<Context> for PageInfo {
    fn endCursor(&self, responder: earl::string::OptionalResponder<Context>) -> earl::Ack {
        responder.ok(self.end_cursor.as_ref())
    }

    fn hasNextPage(&self, responder: earl::boolean::Responder<Context>) -> earl::Ack {
        responder.ok(self.end_cursor.is_some())
    }
}

#[derive(Debug, Clone)]
struct ProjectListingConnection {
    after: Option<String>,
    page_info: PageInfo,
    total_count: i64,
}

impl schema::project_listing_connection::Instance<Context> for ProjectListingConnection {
    fn pageInfo(&self, responder: schema::page_info::Responder<Context>) -> earl::Ack {
        responder.ok(&self.page_info)
    }

    fn totalCount(&self, responder: earl::int::Responder<Context>) -> earl::Ack {
        let ctx = responder.context().clone();
        let user = ctx.auth()?;
        responder.lazy(async move {
            ctx.db
                .handle(move |con: &mut rusqlite::Connection| {
                    con.query_row_named(
                        "select count(*) as count from project where user = :user",
                        rusqlite::named_params![":user": &user],
                        |row| row.get::<_, i64>(0),
                    )
                })
                .await
                .map_err(Into::into)
        })
    }

    fn nodes(&self, responder: schema::project::ListResponder<Context>) -> earl::Ack {
        let ctx = responder.context().clone();
        let user = ctx.auth()?;
        responder.lazy(async move {
            ctx.db
                .handle(move |con: &mut rusqlite::Connection| {
                    let mut stmt =
                        con.prepare("select id, data from project where user = :user")?;
                    let result = stmt.query_and_then_named(
                        rusqlite::named_params![":user": &user],
                        |row| {
                            let mut project: Project =
                                serde_json::from_str(&row.get::<_, String>(1)?)?;
                            project.id = row.get(0)?;
                            Ok(project)
                        },
                    )?;
                    result.collect::<Result<_, error::Error>>()
                })
                .await
                .map_err(Into::into)
        })
    }
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
struct DailyProgress {
    from: u64,
    daily_progress: u64,
}

impl DailyProgress {
    fn from_input(input: &schema::DailyProgressInput) -> Result<Self, earl::Error> {
        Ok(DailyProgress {
            from: input.from as u64,
            daily_progress: input.dailyProgress as u64,
        })
    }
}

impl schema::daily_progress::Instance<Context> for DailyProgress {
    fn from(&self, responder: earl::int::Responder<Context>) -> earl::Ack {
        responder.ok(self.from as i64)
    }
    fn dailyProgress(&self, responder: earl::int::Responder<Context>) -> earl::Ack {
        responder.ok(self.daily_progress as i64)
    }
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
struct Plan {
    due_by: chrono::DateTime<chrono::FixedOffset>,
    started_at: chrono::DateTime<chrono::FixedOffset>,
    daily_progress: OneOrTwo<DailyProgress>,
}

impl Plan {
    fn from_input(input: schema::PlanInput) -> Result<Self, earl::Error> {
        let schema::PlanInput {
            dueBy: due_by,
            startedAt: started_at,
            dailyProgress: daily_progress,
        } = input;
        let daily_progress = match daily_progress.len() {
            0 => {
                return Err(earl::Error::invalid_argument("Empty lists".to_string())
                    .at_field("dailyProgress"))
            }
            1 => OneOrTwo::one(DailyProgress::from_input(&daily_progress[0])?),
            2 => OneOrTwo::two(
                DailyProgress::from_input(&daily_progress[0])?,
                DailyProgress::from_input(&daily_progress[1])?,
            ),
            _ => {
                return Err(
                    earl::Error::invalid_argument("Too many elements".to_string())
                        .at_field("dailyProgress"),
                )
            }
        };
        Ok(Plan {
            due_by: due_by.datetime,
            started_at: started_at.datetime,
            daily_progress,
        })
    }
}

impl schema::plan::Instance<Context> for Plan {
    fn dueBy(&self, responder: schema::datetime::Responder<Context>) -> earl::Ack {
        responder.ok(&schema::Datetime::from(self.due_by))
    }

    fn startedAt(&self, responder: schema::datetime::Responder<Context>) -> earl::Ack {
        responder.ok(&schema::Datetime::from(self.started_at))
    }

    fn dailyProgress(
        &self,
        responder: schema::daily_progress::ListResponder<Context>,
    ) -> earl::Ack {
        responder.ok(self.daily_progress.as_ref())
    }
}

#[derive(Debug, Clone)]
struct Progress {
    timestamp: u64,
    value: Option<u64>,
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
struct Pattern {
    width: u32,
    height: u32,
    data: Vec<u8>,
    colors: Vec<schema::Color>,
}

impl Pattern {
    fn from_input(input: schema::PatternInput) -> Result<Self, earl::Error> {
        use std::convert::TryFrom;
        let schema::PatternInput {
            colors,
            data,
            width,
            height,
        } = input;
        let num_colors = u8::try_from(colors.len()).map_err(|_e| {
            earl::Error::invalid_argument("Only 255 colors are allowed".to_string())
                .at_field("colors")
        })?;

        Ok(Pattern {
            width: width as u32,
            height: height as u32,
            data: data
                .iter()
                .enumerate()
                .map(|(n, i)| {
                    u8::try_from(*i)
                        .map_err(|_e| {
                            earl::Error::invalid_argument("Out of range".to_string()).at_index(n)
                        })
                        .and_then(|i| {
                            if i < num_colors {
                                Ok(i)
                            } else {
                                Err(
                                    earl::Error::invalid_argument("Out of color range".to_string())
                                        .at_index(n),
                                )
                            }
                        })
                })
                .collect::<Result<Vec<u8>, earl::Error>>()
                .map_err(|e| e.at_field("data"))?,
            colors,
        })
    }
}

impl schema::pattern::Instance<Context> for Pattern {
    fn width(&self, responder: earl::int::Responder<Context>) -> earl::Ack {
        responder.ok(self.width as i64)
    }

    fn height(&self, responder: earl::int::Responder<Context>) -> earl::Ack {
        responder.ok(self.height as i64)
    }

    fn data(&self, responder: earl::int::ListResponder<Context>) -> earl::Ack {
        responder.ok(&self
            .data
            .iter()
            .copied()
            .map(Into::into)
            .collect::<Vec<_>>())
    }

    fn colors(&self, responder: schema::color::ListResponder<Context>) -> earl::Ack {
        responder.ok::<Color>(self.colors.as_ref())
    }
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
struct Project {
    #[serde(skip)]
    id: Uuid,
    pattern: Pattern,
    name: String,
    plan: Option<Plan>,
    #[serde(skip)]
    progress: Vec<Progress>,
}

impl schema::progress::Instance<Context> for Progress {
    fn timestamp(&self, responder: earl::int::Responder<Context>) -> earl::Ack {
        responder.ok(self.timestamp as i64)
    }

    fn value(&self, responder: earl::int::OptionalResponder<Context>) -> earl::Ack {
        responder.ok(self.value.as_ref().map(|i| *i as i64))
    }
}

impl schema::project::Instance<Context> for Project {
    fn id(&self, responder: earl::id::Responder<Context>) -> earl::Ack {
        responder.ok(&earl::ID::from(self.id.to_string()))
    }

    fn lastUpdate(&self, responder: earl::int::OptionalResponder<Context>) -> earl::Ack {
        let id = self.id.clone();
        let ctx = responder.context().clone();
        responder.lazy(async move {
            ctx.db
                .handle(move |con| {
                    con.query_row_named(
                    "select timestamp from progress where id = :id order by timestamp desc limit 1",
                    named_params![
                        ":id": id
                    ],
                    |row|{
                        row.get::<_,i64>(0)
                    }
                ).optional()
                })
                .await
                .map_err(Into::into)
        })
    }

    fn name(&self, responder: earl::string::Responder<Context>) -> earl::Ack {
        responder.ok(&self.name)
    }

    fn plan(&self, responder: schema::plan::OptionalResponder<Context>) -> earl::Ack {
        responder.ok(self.plan.as_ref())
    }

    fn progress(
        &self,
        responder: schema::progress_listing_connection::Responder<Context>,
    ) -> earl::Ack {
        let id = self.id.clone();
        let ctx = responder.context().clone();
        responder.lazy(async move {
            let data = ctx.db.handle(move |con|{
                let mut stmt = con.prepare_cached("select timestamp, value from progress where id = :id order by timestamp asc")?;
                let result = stmt.query_map_named(named_params![
                    ":id": id
                ], |row|{
                    Ok(Progress{timestamp: row.get::<_,i64>(0)? as u64, value: row.get::<_,Option<i64>>(1)?.map(|i| i as u64 ) })
                })?;
                result.collect::<Result<Vec<Progress>,_>>()
            }).await?;
            Ok(ProgressListing{progress: data})
        })
    }

    fn pattern(&self, responder: schema::pattern::Responder<Context>) -> earl::Ack {
        responder.ok(&self.pattern)
    }
}

struct ProgressListing {
    progress: Vec<Progress>,
}

impl schema::progress_listing_connection::Instance<Context> for ProgressListing {
    fn nodes(&self, responder: schema::progress::ListResponder<Context>) -> earl::Ack {
        responder.ok(&self.progress)
    }

    fn pageInfo(&self, responder: schema::page_info::Responder<Context>) -> earl::Ack {
        responder.ok(&PageInfo { end_cursor: None })
    }
}

impl schema::query::Instance<Context> for () {
    fn projects(
        &self,
        after: Option<String>,
        responder: schema::project_listing_connection::Responder<Context>,
    ) -> earl::Ack {
        responder.ok(&ProjectListingConnection {
            after,
            page_info: PageInfo { end_cursor: None },
            total_count: 0,
        })
    }
    fn project(
        &self,
        id: earl::ID,
        responder: schema::project::OptionalResponder<Context>,
    ) -> earl::Ack {
        let uuid = Uuid::parse_str(id.as_ref()).map_err(|e| earl::Error::from(e).at_field("id"))?;
        let ctx = responder.context().clone();
        let user = ctx.auth()?;
        responder.lazy(async move {
            let result: Result<Option<Project>, connpool::Error<error::Error>> = ctx
                .db
                .handle(move |con: &mut rusqlite::Connection| {
                    let mut stmt = con.prepare(
                        "select id, data from project where user = :user and id = :id limit 1",
                    )?;
                    let mut result = stmt.query_and_then_named(
                        rusqlite::named_params![":user": user, ":id": uuid],
                        |row| {
                            let mut project: Project =
                                serde_json::from_str(&row.get::<_, String>(1)?)?;
                            project.id = row.get(0)?;
                            Ok(project)
                        },
                    )?;
                    result.next().transpose()
                })
                .await;
            result.map_err(Into::into)
        })
    }
}

fn parse_project_input(input: schema::ProjectInput) -> Result<Project, earl::Error> {
    let schema::ProjectInput {
        id,
        pattern,
        name,
        plan,
    } = input;
    let uuid = Uuid::parse_str(id.as_ref()).map_err(|e| earl::Error::from(e).at_field("id"))?;
    let pattern = Pattern::from_input(pattern).map_err(|e| e.at_field("pattern"))?;
    let plan = plan
        .map(|p| Plan::from_input(p))
        .transpose()
        .map_err(|e| e.at_field("plan"))?;
    Ok(Project {
        id: uuid,
        name,
        pattern,
        plan,
        progress: Vec::new(),
    })
}

struct LastProgress(Option<u64>);

impl earl::extensions::Extension for LastProgress {
    fn key() -> &'static str {
        "lastProgress"
    }
    fn encode(self) -> serde_json::Value {
        self.0.map(|i| i.into()).unwrap_or_default()
    }
}

impl schema::mutation::Instance<Context> for () {
    fn createProject(
        &self,
        input: schema::ProjectInput,
        responder: schema::project::OptionalResponder<Context>,
    ) -> earl::Ack {
        let project = parse_project_input(input).map_err(|e| e.at_field("input"))?;
        let ctx = responder.context().clone();
        let user = ctx.auth()?;
        responder.lazy(async move {
            let id = project.id.clone();
            let data = serde_json::to_string(&project)?;
            ctx.db
                .handle(move |con| {
                    con.execute_named(
                        "insert into project (id,user,data) values (:id,:user,:data)",
                        &[(":id", &id), (":user", &user), (":data", &data)],
                    )
                })
                .await?;
            Ok(Some(project))
        })
    }

    fn updateProject(
        &self,
        id: earl::ID,
        last_update: Option<i64>,
        input: Vec<schema::UpdateProjectInput>,
        responder: earl::int::Responder<Context>,
    ) -> earl::Ack {
        let last_update = last_update
            .map(|i| {
                if i < 0 {
                    Err(
                        earl::Error::invalid_argument(format!["Negative timestampe: {}", i])
                            .at_field("lastUpdate"),
                    )
                } else {
                    Ok(i as u64)
                }
            })
            .transpose()?;
        let uuid = Uuid::parse_str(id.as_ref()).map_err(|e| earl::Error::from(e).at_field("id"))?;
        let mut progress = input
            .iter()
            .enumerate()
            .map(|(i, update)| {
                if update.timestamp <= 0 {
                    return Err(earl::Error::invalid_argument(format![
                        "Negative timestampe: {}",
                        update.timestamp
                    ])
                    .at_index(i)
                    .at_field("timestamp"));
                }
                if let Some(progress) = update.value {
                    if progress <= 0 {
                        return Err(earl::Error::new(earl::ErrorKind::Query)
                            .at_index(i)
                            .at_field("value"));
                    }
                }
                Ok(Progress {
                    timestamp: update.timestamp as u64,
                    value: update.value.as_ref().map(|i| *i as u64),
                })
            })
            .collect::<Result<Vec<_>, earl::Error>>()?;
        progress.sort_by(|a, b| a.timestamp.cmp(&b.timestamp));
        let oldest_progress = progress
            .get(0)
            .ok_or_else(|| earl::Error::new(earl::ErrorKind::Query).at_field("input"))?
            .clone();
        let youngest_progress = progress.last().map(|p| p.timestamp as i64).unwrap();
        {
            let ctx = responder.context().clone();
            responder.lazy(async move {
                let r : Result<_,earl::Error> = ctx.db.handle(move |con|{
                    let tx = con.transaction()?;
                    {
                        let last_timestamp = tx.query_row_named(
                            "select timestamp from progress where id = :id order by timestamp desc limit 1",
                            named_params![
                                ":id": uuid
                            ],
                            |row|{
                                Ok(row.get::<_,i64>(0)? as u64)
                            }
                        ).optional()?;

                        if let Some(u) = last_update {
                            if Some(u) != last_timestamp {
                                return Err(earl::Error::with_message("last timestamp is not in sync").with_extension(LastProgress(last_timestamp)))
                            }
                        }
                        if let Some(u) = last_timestamp {
                            if u >= oldest_progress.timestamp {
                                return Err(earl::Error::with_message("progress rejected").with_extension(LastProgress(last_timestamp)))
                            }
                        }
                    }
                    {
                        let mut stmt = tx.prepare_cached("insert into progress (id,timestamp,value) values (:id,:timestamp,:value)")?;
                        for prg in progress.iter() {
                            stmt.execute_named(named_params![
                                ":id": uuid,
                                ":timestamp": prg.timestamp as i64,
                                ":value": prg.value.as_ref().map(|u| *u as i64)
                            ])?;
                        }
                    }
                    tx.commit()?;
                    Ok(())
                }).await.map_err(|e| e.transpose(|| earl::Error::with_message("Internal") ) );
                r?;
                Ok(youngest_progress)
            })
        }
    }
}
