'use strict';

import * as React from "react";
import { withRouter } from "react-router";

import { Match, RouterHistory } from "react-router-dom";

function WithRouter(props: {match: Match;history: RouterHistory;children: (history: RouterHistory, match: Match) => React.ReactElement<any>;}): React.ReactElement<any> {
  return props.children(props.history, props.match);
}

export default withRouter(WithRouter);