'use strict';


import * as React from "react";
import Alert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import FacebookIcon from "@material-ui/icons/Facebook";
import { makeStyles } from "@material-ui/styles";
import { useLocation } from "react-router-dom";
import Db from "./Db";

import { StartLogin } from "./types";
import * as config from "./config";

const endPoint = config.host + "/auth/start";
const useLoginStyles = makeStyles({
  root: {
    display: "flex",
    flexFlow: "column",
    flexBasis: "100%",
    height: "100%"
  },
  fb: {
    backgroundColor: "#3b5998",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#8b9dc3"
    }
  },
  container: {
    "& > *": {
      margin: "15px"
    }
  },
  indie: {
    "& button": {
      borderRadius: "0px",
      backgroundColor: "#5FA328",
      color: "#fff"
    },
    "& .MuiInputBase-root": {
      flexGrow: 1,
      padding: "6px 16px"
    },
    display: "flex"
  }

});

export type Props = {
  error?: string;
};

const Start = (props: {startLogin: StartLogin;setError: (arg0: any) => void;}): React.ReactNode => {
  const {
    startLogin,
    setError
  } = props;
  const [redirect, setRedirect] = React.useState<string | null | undefined>();
  React.useEffect(() => {
    fetch(endPoint, {
      body: JSON.stringify(startLogin),
      method: "POST"
    }).then(d => d.json()).then(r => setRedirect(r.redirect)).catch(setError);
  }, []);
  if (redirect) {
    window.location = redirect;
  }
  return <Db.LoadingFull />;
};
const OnlyForTesting = (props: {children: any;}): React.ReactNode => {
  if (process.env.NODE_ENV === "production") {
    return null;
  } else {
    return <>{props.children}</>;
  }
};

export default ((props: Props): React.ReactNode => {
  const location = useLocation();
  const [error, setError] = React.useState(props.error);
  const [startLogin, setStartLogin] = React.useState<StartLogin | null | undefined>();
  const [redirect, setRedirect] = React.useState<string | null | undefined>();
  const [indieAuth, setIndieAuth] = React.useState<string | null | undefined>();
  const styles = useLoginStyles();
  if (startLogin) {
    return <Start startLogin={startLogin} setError={e => {setStartLogin(null);setError(e);}} />;
  } else {
    let errorMessage = null;
    if (error) {
      let description;
      if (error instanceof TypeError) {
        description = "N001: Network error. Please check your connection.";
      } else if (typeof error === "string") {
        description = error;
      } else {
        throw error;
      }
      errorMessage = <Grid item xs={12}>
                <Alert variant="filled" severity="error">
                {description}
                </Alert>
            </Grid>;
    } else if ((location.state || {}).loginCancelled) {
      errorMessage = <Grid item xs={12}>
                <Alert variant="filled" severity="info">
                Login cancelled
                </Alert>
            </Grid>;
    }
    return <Grid container className={styles.container}>
                {errorMessage}
                <Grid item xs={12}>
                    <Paper className={styles.indie} variant="outlined">
                    <InputBase label="IndieAuth" required placeholder="Login with IndieAuth" onChange={ev => setIndieAuth(ev.target.value)} />
                    <Button onClick={() => setStartLogin({ provider: "indie-auth", me: indieAuth })} variant="contained"><LockOpenIcon /></Button>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Button data-cy="oauth2-facebook" onClick={() => setStartLogin({ provider: "oauth2", name: "facebook" })} fullWidth variant="contained" className={styles.fb} startIcon={<FacebookIcon />}>Login with Facebook</Button>
                </Grid>
                <OnlyForTesting>
                        <Grid item xs={12}>
                            <Button data-cy="foo" onClick={() => setStartLogin({ provider: "foo", id: "ba2e4648-26f8-420b-a562-e050d00254b1" })} fullWidth variant="contained">Foo</Button>
                        </Grid>
                        <Grid item xs={12}>
                            <Button data-cy="fail" onClick={() => setStartLogin({ provider: "fail", error: "access_denied" })} fullWidth variant="contained">Fail</Button>
                        </Grid>
                </OnlyForTesting>
            </Grid>;
  }
});