"use strict";

import * as React from "react";
import { Redirect } from "react-router";

import { makeStyles } from "@material-ui/styles";

import AppBar from "@material-ui/core/AppBar";
import Avatar from "@material-ui/core/Avatar";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";

import AddIcon from "@material-ui/icons/Add";
import CheckIcon from "@material-ui/icons/Check";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import Db from "./Db";
import Db2 from "./Db2";
import FabGroup from "./FabGroup";
import WithRouter from "./WithRouter";
import ProjectImg from "./ProjectImg";

import { useHistory, useParams } from "react-router-dom";
import { Pattern, Project, Plan, Change } from "./types";
import renderCanvas from "./renderCanvas";

function formatDate(date: Date | null | undefined): string | null | undefined {
  if (date == null) {
    return null;
  }
  return date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + date.getDate();
}

function parseDate(value: string | null | undefined): Date | null | undefined {
  if (value) {
    return new Date(value + " 23:59:59");
  } else {
    return null;
  }
}

const usePatternColors = makeStyles({
  root: props => {
    const classes = {};
    props.colors.forEach((color, i) => {
      classes["& .color" + i] = {
        border: "2px solid gray",
        backgroundColor: color.slice(0, -2)
      };
    });
    return classes;
  }
});

const ColorList = React.memo(function ColorList(props: {pattern: Pattern;}): React.ReactElement<List> {
  const {
    pattern
  } = props;
  const freq = Array.from(pattern.colors, _ => 0);
  pattern.data.forEach(c => {
    freq[c] = freq[c] + 1;
  });
  const sum = freq.reduce((a, b) => a + b);
  const classes = usePatternColors(pattern);
  return <List>
        {pattern.colors.map((color, i) => <ListItem key={i} className={classes["root"]}>
                <ListItemAvatar>
                    <Avatar className={"color" + i}>&nbsp;</Avatar>
                </ListItemAvatar>
                <ListItemText primary={freq[i] + " squares / " + 100 * freq[i] / sum + "%"} />
            </ListItem>)}
    </List>;
});

function calculatePlan(dueBy: Date, currentProgress: number, targetProgess: number): Plan {
  const now = new Date();
  const diff = dueBy - now;
  if (diff <= 0) {
    throw "DueBy date is in the past";
  }
  let days = Math.round(diff / 86400000);
  if (days <= 0) {
    days = 1;
  }
  const squaresToGo = targetProgess - currentProgress;
  const remainder = squaresToGo % days;
  const daily = (squaresToGo - remainder) / days;
  let dailyProgress = [{
    from: currentProgress + remainder * (daily + 1),
    dailyProgress: daily
  }];
  if (remainder > 0) {
    dailyProgress.unshift({
      from: currentProgress,
      dailyProgress: daily + 1
    });
  }
  return {
    dueBy: dueBy,
    dailyProgress: dailyProgress,
    startedAt: now
  };
}

function updatePlan(project: Project, dueBy: Date | null | undefined, currentProgress: number): Project {
  let nu: Project = Object.assign({}, project);
  if (dueBy == null) {
    delete nu.plan;
  } else {
    nu.plan = calculatePlan(dueBy, currentProgress, project.pattern.data.length);
  }
  return nu;
}

function SaveProject(props: {project: Project;}): React.ReactElement<any> {
  const {
    project
  } = props;
  const db = Db2.useDb();
  const result = db.saveProject(project);
  if (result.result) {
    return <Redirect to={"/project/" + project.localId} />;
  } else {
    return <Db.LoadingFull />;
  }
}

export function ProjectSettings(props: {project: Project;progress: number;}): React.ReactElement<any> {
  const {
    project,
    progress
  } = props;
  const history = useHistory();
  const edit = {};
  const [projectState, setProject] = React.useState(project);
  const [isSaving, setSaving] = React.useState(false);
  const [colorsIsOpen, setColorsIsOpen] = React.useState(false);
  const onClick = React.useCallback(ev => {
    ev.preventDefault();
    setSaving(true);
  }, [setSaving]);
  if (isSaving) {
    return <SaveProject project={projectState} />;
  }
  let planView = null;
  if (projectState.plan) {
    planView = <ListItem>
            <Typography variant="body2">
                {projectState.plan.dailyProgress.map(x => x.dailyProgress).join("/")} squares per day
            </Typography>
        </ListItem>;
  }
  return <form>
    <AppBar position="static">
    <Toolbar>
        <IconButton edge="start" color="inherit" aria-label="menu" onClick={ev => history.push("/project/" + project.localId)}>
            <ArrowBackIcon />
        </IconButton>
        <Typography variant="h6">
        Settings
        </Typography>
    </Toolbar>
    </AppBar>
    <Grid container>
    <Grid item xs={12} sm={8}>
        <div style={{ maxWidth: "75vw", maxHeight: "75vh", margin: "auto" }}>
            <ProjectImg pattern={project.pattern} progress={0} />
        </div>
        </Grid>
        <Grid item xs={12} sm={4}>
            <List>
                <ListItem>
                    <TextField label="Name" data-cy="name" fullWidth required defaultValue={projectState.name} onChange={ev => setProject(p => {
              p.name = ev.target.value;
              return p;
            })} />
                </ListItem>
                <ListItem>
                    <ListItemText secondary={project.pattern.width + " x " + project.pattern.height + " squares"} />
                </ListItem>
                <Divider />
                <ListItem onClick={ev => setColorsIsOpen(!colorsIsOpen)}>
                    <ListItemText primary={project.pattern.colors.length + " different colors"} />
                    {colorsIsOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={colorsIsOpen}>
                    <ColorList pattern={project.pattern} />
                </Collapse>
                <Divider />
                <ListItem>
                    <TextField type="date" label="Due date" fullWidth data-cy="dueDate" fullWidth required inputProps={{ min: formatDate(new Date()) }} InputLabelProps={{ shrink: true }} defaultValue={project.plan ? formatDate(project.plan.dueBy) : ""} onChange={ev => setProject(updatePlan(projectState, parseDate(ev.target.value), progress))} />
                </ListItem>
                {planView}
            </List>
        </Grid>
    </Grid>
    <FabGroup>
        <Fab color="primary" aria-label="Ok" onClick={onClick}>
             <CheckIcon />
        </Fab>
    </FabGroup>
  </form>;
}

export default function (props: {}): React.ReactElement<any> {
  const {
    project
  } = useParams();
  const db = Db2.useDb();
  const projectAndProgress = db.getProjectAndProgress(project);
  if (projectAndProgress.result) {
    const [project, progress] = projectAndProgress.result;
    return <ProjectSettings project={project} progress={progress.value} />;
  } else {
    return <Db.LoadingFull />;
  }
};