'use strict';


export type uuid = string;

export type Pattern = {
  width: number;
  height: number;
  colors: Array<string>;
  data: Array<number>;
};

export type DailyProgess = {
  from: number;
  dailyProgress: number;
};

export type Plan = {
  dueBy: Date;
  dailyProgress: [DailyProgess] | [DailyProgess, DailyProgess];
  startedAt: Date;
};

export type Project = {
  localId: string;
  id?: uuid;
  name: string;
  pattern: Pattern;
  plan?: Plan;
  user?: uuid;
};

export type Change = {
  name?: string;
  plan?: Plan;
};

export type Progress = {
  project: string;
  date: number;
  value?: number;
  change?: Change;
};

export type StartLogin = {
  provider: string;
  me?: string;
};

export type User = {
  id: uuid;
  lastUsed: number;
  name: string;
  jwt: string;
  startLogin: StartLogin;
};
