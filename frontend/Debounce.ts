"use strict";


import * as React from "react";

type State<T> = {fired: boolean;props?: T;};

export default function Debounced<T>(timeout: number, component: React.ComponentType<T>): React.ComponentType<T> {
  const result = class Debounced extends React.Component<T, State<T>> {

    _fire: () => void = () => {};
    _timeout: number = -1;

    constructor(props: T) {
      super(props);
      this.state = { fired: false };
      this._fire = this.fire.bind(this);
    }

    componentDidMount() {
      this._timeout = window.setTimeout(this._fire, timeout);
    }

    componentDidUpdate(_, prevState: State<T>) {
      if (prevState.fired) {
        if (this.state === prevState) {
          this.setState({ fired: false });
          window.clearTimeout(this._timeout);
          this._timeout = window.setTimeout(this._fire, timeout);
        }
      } else {
        window.clearTimeout(this._timeout);
        this._timeout = window.setTimeout(this._fire, timeout);
      }
    }

    componentWillUnmount() {
      window.clearTimeout(this._timeout);
    }

    fire() {
      this.setState({ fired: true, props: this.props });
    }

    render(): React.ReactElement<typeof component> | null {
      if (this.state.props) {
        return React.createElement(component, this.state.props);
      }
      return null;
    }
  };
  return result;
}