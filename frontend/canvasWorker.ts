'use strict';


import { Pattern } from "./types";
import render from "./renderCanvas";

onmessage = ev => render(ev.data);