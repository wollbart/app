"use strict";


import * as React from "react";
import { Switch, Route, Link } from "react-router-dom";
import Db2 from "./Db2";

const Truncate = props => {
  const db = Db2.useDb();
  const result = db.send({ method: "__truncate" });
  if (result.result) {
    return <span>Ok</span>;
  } else {
    return <span>Loading</span>;
  }
};

export default (props => {
  return <Switch>
        <Route path="/__foo/truncate" component={Truncate} />
        <Route path="/__foo/login" component={Login} />
    </Switch>;
});