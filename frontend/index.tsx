'use strict';


import * as React from "react";
import ReactDOM from "react-dom";
import * as Sentry from "@sentry/browser";
import { BrowserRouter as Router, Switch, Route, Link, Redirect, useHistory, useLocation } from "react-router-dom";
import { makeStyles } from "@material-ui/styles";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import SettingsIcon from "@material-ui/icons/Settings";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import CssBaseline from "@material-ui/core/CssBaseline";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Match, ContextRouter, RouterHistory } from "react-router-dom";
import SwipeableViews from "react-swipeable-views";
import Grid from "@material-ui/core/Grid";

import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";

import Avatar from "@material-ui/core/Avatar";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";

import * as config from "./config";
import Db from "./Db";
import Db2 from "./Db2";
import FabGroup from "./FabGroup";
import ProjectImg from "./ProjectImg";
import ErrorBoundary from "./ErrorBoundary";

import ProjectView from "./ProjectView";
import ProjectSettings from "./ProjectSettings";
import PatternImporter from "./PatternImporter";
import Foo from "./Foo";
import Login from "./Login";

import View from "./project/View.svelte";

import { Project } from "./types";

import DbS from './Db.svelte';
import { setContext } from 'svelte';
import {
	create_component,
    	destroy_component,
	mount_component,
	transition_in,
	transition_out
} from "svelte/internal";
 
function formatQuarter(quat) {
  switch (quat) {
    case 1:case -1:
      return "¼";
    case 2:case -2:
      return "½";
    case 3:case -3:
      return "¾";

  }
  return "";
}

function calculatePlan(project: Project, progress: number): string | null | undefined {
  const plan = project.plan;
  if (!plan) {
    return null;
  }
  const now = new Date();
  const size = project.pattern.data.length;
  const daysLeft = Math.round((plan.dueBy - now) / 86400000);
  const [first, second] = plan.dailyProgress;
  let progressLeft;
  if (second) {
    if (progress >= second.from) {
      progressLeft = (size - progress) / second.dailyProgress;
    } else {
      progressLeft = (second.from - progress) / first.dailyProgress + (size - second.from) / second.dailyProgress;
    }
  } else {
    progressLeft = (size - progress) / first.dailyProgress;
  }
  const behindPlan = progressLeft - daysLeft;
  const quatDays = Math.round(behindPlan * 4);
  const quarters = quatDays % 4;
  const fullDays = (quatDays - quarters) / 4;
  if (fullDays >= 1) {
    return fullDays + formatQuarter(quarters) + " days behind";
  } else if (fullDays <= -1) {
    return -fullDays + formatQuarter(quarters) + " days ahead";
  }
  return "In plan";
}

const useProjectStyle = makeStyles({
  navCenter: {
    flexGrow: 1
  },
  root: {
    display: "flex",
    flexFlow: "column",
    flexBasis: "100%",
    height: "100%",
    "& .cardHeader": {
      display: "flex"
    },
    "& .cardContent": {
      flexGrow: "1",
      "& .rightAlign": {
        position: "absolute",
        right: "16px"
      }
    },
    "& .progress": {
      display: "flex",
      flexFlow: "row",
      justifyContent: "center",
      "& > div": {
        padding: "0px 8px"
      },
      "& > .score": {
        borderRight: "1px solid rgba(0,0,0,0.12)",
        borderLeft: "1px solid rgba(0,0,0,0.12)"
      }
    }
  },
  swiper: {
    padding: "0px 10%",
    flexGrow: "1",
    "@media (orientation: landscape)": {
      padding: "0px 50% 0px 0px"
    },
    "& > .react-swipeable-view-container": {
      height: "calc(100% - 24px)"
    },
    "& > .react-swipeable-view-container > div": {
      display: "flex"
    },
    "& .MuiCard-root": {
      flexGrow: "1",
      margin: "10px",
      flexFlow: "column",
      display: "flex"
    }
  }
});

const Score = function (props: {from: number;to: number;}): React.ReactElement<any> {
  const {
    from,
    to
  } = props;
  const [state, setState] = React.useState(from);
  React.useEffect(() => {
    let current = from;
    const ticker = window.setInterval(() => {
      current = current + Math.ceil((to - current) / 10);
      if (current == to) {
        window.clearInterval(ticker);
      }
      setState(current);
    }, 100);
    return () => window.clearInterval(ticker);
  }, [from, to]);
  const added = from - state;
  return <Typography variant="h3" component="span">{state}</Typography>;
};

const ProjectCard = (props: {project: Project;}): React.ReactElement<any> => {
  const {
    project
  } = props;
  const history = useHistory();
  const db = Db2.useDb();
  const progress = db.getProgressAndStreak(project.localId);
  let content = null;
  if (progress.result) {
    const [latest, yesterday] = progress.result;
    const progress_percentage = latest.value / project.pattern.data.length * 100;
    content = <List className="cardContent">
                <ListItem key="progress" className="progress">
                    <div>
                        <Typography variant="body2" component="span">{Math.floor(progress_percentage)}%</Typography>
                    </div>
                    <div className="score">
                        <Score from={yesterday.value} to={latest.value} />
                    </div>
                    <div>
                        <Typography variant="body2" component="span"><ArrowDropUpIcon />{latest.value - yesterday.value}</Typography>
                    </div>
                </ListItem>
                <ListItem key="plan">
                    <ListItemText primary={calculatePlan(project, latest.value)} />
                </ListItem>
            </List>;
  }
  return <Card>
            <CardActionArea onClick={event => {history.push("/project/" + project.localId);}}>
                <ProjectImg pattern={project.pattern} progress={0} />
                <CardContent className="cardHeader">
                    <Typography gutterBottom variant="h5" component="h2">{project.name}</Typography>
                </CardContent>
            </CardActionArea>
            <Divider />
            {content}
            <CardActions disableSpacing>
                <IconButton onClick={event => {history.push("/project/" + project.localId + "/settings");}}>
                  <SettingsIcon />
                </IconButton>
            </CardActions>
        </Card>;
};


const Projects = function (props: {openLogin: boolean;}): React.ReactElement<any> {
  const history = useHistory();
  const styles = useProjectStyle();
  const last24hours = Date.now() - 86400000;
  const db = Db2.useDb();
  const projects = db.getProjects();
  const currentUser = db.currentUser();
  if (projects.loading) {
    return <Db.LoadingFull />;
  }
  if (projects.error) {
    throw projects.error;
  }
  let userArea;
  if (currentUser.result) {
    if (currentUser.result.id) {
      userArea = <>
                <Typography variant="subtitle1" component="span" color="inherit" data-cy="currentUser.name">
                    {(currentUser.result || {}).name} 
                </Typography>
                <AccountCircleIcon />
                </>;
    } else {
      userArea = <Button color="inherit" onClick={() => history.push("/login")}>Login</Button>;
    }
  }
  return <div className={styles.root}>
        <AppBar position="static">
            <Toolbar>
                <span className={styles.navCenter} />
                {userArea}
            </Toolbar>
        </AppBar>
        <SwipeableViews enableMouseEvents resistance className={styles.swiper} data-cy="projects">
        {projects.result.map(data => <ProjectCard key={data.localId} project={data} data-cy="project" />)}
        </SwipeableViews>
        <FabGroup>
            <Fab color="primary" aria-label="Add" onClick={event => {
        history.push("/pattern-import");
      }}>
             <AddIcon />
            </Fab>
        </FabGroup>
        <Dialog open={!!props.openLogin} onClose={() => history.push("/")} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Login</DialogTitle>
            <Login />
        </Dialog>
    </div>;
};

const ShowProject = function (props: {match: Match;}): React.ReactElement<any> {
  const id = props.match.params["project"];
  const db = Db2.useDb();
  const result = db.getProjectAndProgress(id);
  if (result.result) {
    return <ProjectView data={result.result[0]} progress={result.result[1].value} />;
  } else {
    return <Db.LoadingFull />;
  }
};

const AuthLogin = (props: {jwt: string;name: string;}): React.ReactElement<any> => {
  const db = Db2.useDb();
  const login = db.login(props.jwt, props.name, {});
  if (login.loading) {
    return <Db.LoadingFull />;
  } else if (login.result) {
    return <Redirect to="/" />;
  } else {
    throw <div>{JSON.stringify(login.error)}</div>;
  }
};

const AuthVerify = (props: {state: string;code: string;}): React.ReactElement<any> => {
  const {
    state,
    code
  } = props;
  const [response, setResponse] = React.useState({});
  React.useEffect(() => {
    const endPoint = config.host + "/auth/verify";
    fetch(endPoint, {
      body: JSON.stringify({ state, code }),
      method: "POST"
    }).then(r => r.json()).then(j => setResponse({ result: j })).catch(reason => {
      setResponse({ error: reason });
    });
  }, [state, code]);
  if (response.result) {
    return <AuthLogin jwt={response.result.key} name={response.result.name} />;
  } else if (response.error) {
    throw response.error;
  } else {
    return <Db.LoadingFull />;
  }
};

const Auth = function (props: {}): React.ReactElement<any> {
  const location = useLocation();
  if (location.state) {
    return <AuthVerify state={location.state.state} code={location.state.code} />;
  }
  const params = new URLSearchParams(location.search);
  const code = params.get("code");
  const state = params.get("state");
  const error = params.get("error");
  if (error) {
    if (error == "access_denied") {
      // Cancelled by user
      return <Redirect to={{ pathname: "/login", state: { loginCancelled: true } }} />;
    } else {
      throw "L001: " + error;
    }
  } else if (code && state) {
    return <Redirect to={{ pathname: "/auth", state: { state, code } }} />;
  } else {
    throw "L000: /auth started without error,state and code parameter.";
  }
};

const Logout = (props: {}): React.ReactElement<any> => {
  const db = Db2.useDb();
  const result = db.logout();
  if (result.loading) {
    return <Db.LoadingFull />;
  } else if (result.result) {
    return <Redirect to="/" />;
  } else {
    throw result.error;
  }
};

const svelte = function (component: class): React.ReactElement<any> {
  	return (props) => {
        	const create_default_slot = (ctx) {
            		let other;
            		let current;
            		other = new component({props: props, $$inline: true});
            		return {
        			c() {
                			create_component(other.$$.fragment);
                    		},
        			m(target, anchor) {
        				mount_component(other, target, anchor);
                			current = true;
        			},
        			i(local) {
        				if (current) return;
        				transition_in(other.$$.fragment, local);
        				current = true;
        			},
        			o(local) {
        				transition_out(other.$$.fragment, local);
                    			current = false;
        			},
        			d(detaching) {
        				destroy_component(other, detaching);
        			}
                	};
        	}
 		const elem = React.useRef();
		const db = Db2.useDb();
		React.useEffect(() => {
			const c = new DbS({
				target: elem.current,
				props: {db: db, $$slots: {default: [create_default_slot]}, $$scope:{ctx: []}}
			});
		});
		return <div ref={elem}></div>;
	}
}

const SView = svelte(View);

const App = function (): React.ReactElement<any> {
  return <>
        <CssBaseline />
        <Router>
        <Switch>
            <Route exact path="/">
                <Projects />
            </Route>
            <Route path="/project/:project/settings" component={ProjectSettings} />
            <Route path="/project/:project" component={ShowProject} />
            <Route path="/projectx/:project" component={SView} />
            <Route path="/pattern-import" component={PatternImporter} />
            <Route path="/__foo/" component={Foo} />
            <Route path="/login">
                <Projects openLogin={true} />
            </Route>
            <Route path="/logout" component={Logout} />
            <Route path="/auth" component={Auth} />
        </Switch>
        </Router>
    </>;
};

if (process.env.NODE_ENV === "production") {
  Sentry.init({
    dsn: "https://3070e2e119634a308edc151c77501661@sentry.io/1316448"
  });

  if (navigator.serviceWorker) {
    const dont = "./service-worker.js";
    navigator.serviceWorker.register(dont).then(console.log, console.error);
  }
}

const container = document.getElementById("container");
if (container != null) {
  ReactDOM.render(<ErrorBoundary><Db2.Open><App /></Db2.Open></ErrorBoundary>, container);
} else {
  console.error("Container not found");
}
