'use strict';

import { VERSION, NAME, upgrade, Store, StoreName, promisify } from "./Db";
import { Type } from "./Db2";
import UUID from "./uuid";
import jwt_decode from "jwt-decode";
import * as Sentry from "@sentry/browser";
import login from "./loginState";
import * as config from "./config";
let db = null;
let error = null;
let port = null;
const endPoint = config.host + "/graphql";
const syncState = {};
const timer = {};
const stash = {};

if (process.env.NODE_ENV === "production") {
  Sentry.init({
    dsn: "https://3070e2e119634a308edc151c77501661@sentry.io/1316448"
  });
} else {
  onunhandledrejection = event => {
    console.warn(`UNHANDLED PROMISE REJECTION: ${event.reason}`, event);
  };
}

const req = indexedDB.open(NAME, VERSION);
req.onupgradeneeded = ev => {
  upgrade(ev);
};
req.onsuccess = ev => {
  const result = ev.target.result;
  const tx = result.transaction([Store.Users], "readonly");
  const promise = new Promise((resolve, reject) => {
    const progress = tx.objectStore(Store.Users);
    const cursor = progress.openCursor();
    let lastUsed = 0;
    let user = null;
    cursor.onerror = ev => {
      reject(ev.target.error);
    };
    cursor.onsuccess = ev => {
      if (ev.target.result) {
        if (ev.target.result.value.lastUsed > lastUsed) {
          lastUsed = ev.target.result.value.lastUsed;
          user = ev.target.result.value;
        }
        ev.target.result.continue();
      } else {
        resolve(user);
      }
    };
  });
  promise.then(user => {
    if (user) {
      const d = jwt_decode(user.jwt);
      login.set({
        id: d.sub,
        exp: d.exp,
        jwt: user.jwt,
        name: user.name
      });
    }
    db = ev.target.result;
    if (port) {
      port.onmessage = onPortMessage;
    }
    syncProjects();
  });
};
req.onerror = ev => {
  error = ev.error;
};

self.onmessage = ev => {
  port = ev.ports[0];
  if (db) {
    port.onmessage = onPortMessage;
  }
};


const subscriptions = {};

const writeTx = (stores, cb) => {
  if (!db) {
    throw "DB is not open yet";
  }
  const tx = db.transaction(stores, "readwrite");
  const promise = promisify(cb(tx));
  const txPromise = new Promise((resolve, reject) => {
    tx.oncomplete = ev => {
      resolve(ev.target.result);
    };
    tx.onerror = ev => {
      reject(ev.error);
    };
  });
  return Promise.all([promise, txPromise]).then(r => r[0]);
};

const readTx = (stores, cb) => {
  if (!db) {
    throw "DB is not open yet";
  }
  const tx = db.transaction(stores, "readonly");
  return promisify(cb(tx));
};

const readAll = function (req: IDBRequest) {
  const result = [];
  return new Promise((resolve, reject) => {
    req.onsuccess = ev => {
      if (ev.target.result) {
        result.push(ev.target.result.value);
        ev.target.result.continue();
      } else {
        // done
        resolve(result);
      }
    };
    req.onerror = ev => {
      reject(ev.error);
    };
  });
};

const progress = async progress => {
  const push = await writeTx([Store.Progress], async tx => {
    const prev = await promisify(tx.objectStore(Store.Progress).openCursor(IDBKeyRange.bound([progress.project, 0], [progress.project, progress.date]), "prev"));
    if (prev && prev.value) {
      const value = prev.value;
      if (progress.date - value.date < 3000) {
        return Promise.resolve(false);
      }
    }
    return promisify(tx.objectStore(Store.Progress).add(progress)).then(() => true);
  });
  if (push) {
    const remoteId = await getRemoteId(progress.project);
    if (remoteId) {
      await pushProgress(remoteId, progress.project);
    }
  }
};

function FetchProgress(id) {
  return FetchProgressAt(id, Number.MAX_VALUE);
}

function FetchProgressAt(id, timestamp) {
  return function (tx) {
    return new Promise((resolve, reject) => {
      const progress = tx.objectStore(Store.Progress);
      const cursor = progress.openCursor(IDBKeyRange.bound([id, 0], [id, timestamp]), "prev");
      cursor.onerror = ev => {
        reject(ev.target.error);
      };
      cursor.onsuccess = ev => {
        if (ev.target.result) {
          if (ev.target.result.value.value) {
            resolve(ev.target.result.value);
          } else {
            ev.target.result.continue();
          }
        } else {
          resolve({ value: 0 });
        }
      };
    });
  };
}

const lastUpdate = (id, tx) => {
  return new Promise((resolve, reject) => {
    const progress = tx.objectStore(Store.Progress);
    const cursor = progress.openKeyCursor(IDBKeyRange.bound([id, 0], [id, Number.MAX_VALUE]), "prev");
    cursor.onerror = ev => {
      reject(ev.target.error);
    };
    cursor.onsuccess = ev => {
      if (ev.target.result) {
        resolve(ev.target.result.key[1]);
      } else {
        resolve(null);
      }
    };
  });
};

const getRemoteId = async id => {
  const project = await readTx([Store.Projects], tx => tx.objectStore(Store.Projects).get(id));
  return project.id;
};

const getProjectAndProgress = async (id: string) => {
  const loginState = login.get();
  return readTx([Store.Projects, Store.Progress], tx => [tx.objectStore(Store.Projects).get(id), FetchProgress(id)(tx)]).then(([project, progress]) => {
    if (project && project.user == loginState.id) {
      return [project, progress];
    } else {
      return [null, null];
    }
  });
};

const getProjects = async () => {
  const loginState = login.get();
  return readTx([Store.Projects], tx => readAll(tx.objectStore(Store.Projects).openCursor()).then(ps => ps.filter(p => p.user == loginState.id)));
};

const saveProject = async (project: Project): Promise<boolean> => {
  const loginState = login.get();
  await writeTx([Store.Projects, Store.Progress], async tx => {
    const oldProject = await promisify(tx.objectStore(Store.Projects).get(project.localId));
    if (oldProject == null) {
      if (loginState.id) {
        project.user = loginState.id;
      }
      await promisify(tx.objectStore(Store.Projects).put(project));
    } else {
      if (oldProject.user != loginState.id) {
        throw "Wrong user";
      }
      const change: Change = {};
      if (oldProject.plan != project.plan) {
        change.plan = project.plan;
      }
      if (oldProject.name != project.name) {
        change.name = project.name;
      }
      await Promise.all([promisify(tx.objectStore(Store.Projects).put(project)), promisify(tx.objectStore(Store.Progress).add({
        project: project.localId,
        date: Date.now(),
        change: change
      }))]);
    }
    return true;
  });
  return true;
};

const getProgressAndStreak = async id => {
  const loginState = login.get();
  return readTx([Store.Progress], tx => new Promise((resolve, reject) => {
    const progress = tx.objectStore(Store.Progress);
    const cursor = progress.openCursor(IDBKeyRange.bound([id, 0], [id, Number.MAX_VALUE]), "prev");
    cursor.onerror = ev => {
      reject(ev.target.error);
    };
    var first = { value: 0 };
    var last = null;
    cursor.onsuccess = ev => {
      if (ev.target.result) {
        if (ev.target.result.value.value) {
          if (last == null) {
            first = last = ev.target.result.value;
          } else {
            if (last.date - ev.target.result.value.date > 4 * 60 * 60 * 1000) {
              resolve([first, ev.target.result.value]);
              return;
            } else {
              last = ev.target.result.value;
            }
          }
        }
        ev.target.result.continue();
      } else {
        resolve([first, last ? last : first]);
      }
    };
  }));
};

const onPortMessage = ev => {
  const data: Message = ev.data;
  switch (data.method) {
    case "__truncate":
      writeTx([Store.Projects, Store.Progress], tx => {
        return Promise.all([Store.Projects, Store.Progress].map(s => {
          promisify(tx.objectStore(s).clear());
        }));
      }).then(() => {
        ev.ports[0].postMessage({ result: true });
      });
      return;
    case Type.Progress:
      stash[data.progress.project] = data.progress;

      if (!timer[data.progress.project]) {
        timer[data.progress.project] = setTimeout(() => {
          timer[data.progress.project] = null;
          progress(stash[data.progress.project]);
        }, 3000);
      }

      return;
    case Type.GetProjectAndProgress:
      getProjectAndProgress(data.id).then(result => {
        ev.ports[0].postMessage({ result: result });
        return result;
      });
      return;
    case Type.GetProgressAndStreak:
      getProgressAndStreak(data.id).then(result => {
        ev.ports[0].postMessage({ result: result });
        return result;
      });
      return;
    case Type.GetProjects:
      getProjects().then(result => {
        ev.ports[0].postMessage({ result: result });
        return result;
      });
      return;
    case Type.SaveProject:
      saveProject(data.project).then(result => {
        ev.ports[0].postMessage({ result: result });
        syncProjects();
      });
      return;
    case Type.Login:
      const jwt = data.jwt;

      try {
        const d = jwt_decode(jwt);
        const loginState = {
          id: d.sub,
          exp: d.exp,
          jwt: jwt,
          name: data.name
        };
        writeTx([Store.Users], tx => {
          return tx.objectStore(Store.Users).put(({
            id: d.sub,
            lastUsed: Date.now(),
            name: data.name,
            jwt: jwt,
            startLogin: data.startLogin
          } as User));
        }).then(() => {
          login.set(loginState);
          ev.ports[0].postMessage({ result: true });
        });
      } catch (e) {
        ev.ports[0].postMessage({ error: e });
      }

      return;
    case Type.Logout:
      writeTx([Store.Users], tx => {
        return tx.objectStore(Store.Users).clear();
      }).then(() => {
        login.reset();
        ev.ports[0].postMessage({ result: true });
      });
      return;
    case Type.CurrentUser:
      const port = ev.ports[0];
      login.listen(next => {
        port.postMessage({ result: { name: next.name, id: next.id } });
      });
      return;

  }
  throw "Unknown event: " + JSON.stringify(data);
};

const syncProjects = async () => {
  const loginState = login.get();
  const query = `
query projects {
  projects {
    nodes {
        id
        lastUpdate
    }
  }
}`;
  if (!loginState.jwt) {
    return;
  }
  try {
    const remoteReq = fetch(endPoint, {
      body: JSON.stringify({
        query: query,
        operationName: "projects"
      }),
      method: "POST",
      headers: { "Authorization": "Bearer " + loginState.jwt }
    });
    const localReq = readTx([Store.Projects, Store.Progress], async tx => {
      const projects = await readAll(tx.objectStore(Store.Projects).openCursor());
      return Promise.all(projects.map((p: Project) => {
        return lastUpdate(p.localId, tx).then(l => {
          return { id: p.id, lastUpdate: l, project: p };
        });
      }));
    });
    const remote = await (await remoteReq).json();
    const local = await localReq;
    const missingRemote = local.filter(l => {
      if (l.id) {
        return remote.data.projects.nodes.findIndex(r => r.id == l.id) == -1;
      } else {
        return true;
      }
    });
    const missingLocal = remote.data.projects.nodes.filter(r => {
      return local.findIndex(l => r.id == l.id) == -1;
    });
    const common = local.map(l => {
      if (l.id) {
        return [l, remote.data.projects.nodes.find(r => r.id == l.id)];
      } else {
        return [l, null];
      }
    }).filter(([l, r]) => r != null);
    await Promise.all(missingRemote.map(l => createProject(l.project)) + missingLocal.map(r => downloadProject(r.id)) + common.map(([l, r]) => {
      syncState[l.project.localId] = r.lastUpdate;
      pushProgress(l.project.id, l.project.localId);
    }));
  } catch (e) {
    Sentry.captureException(e);
    setTimeout(syncProjects, 60 * 10 * 1000);
  }
};

const createProject = async (project: Project) => {
  const loginState = login.get();
  if (!project.id) {
    if (project.localId.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i)) {
      project.id = project.localId;
    } else {
      project.id = UUID();
    }
    await writeTx([Store.Projects], tx => {
      return tx.objectStore(Store.Projects).put(project);
    });
  }
  const id = project.id;
  const query = `
mutation createProject($input: ProjectInput!) {
  createProject(input: $input) {
    id
  }
}`;
  const remoteReq = fetch(endPoint, {
    body: JSON.stringify({
      query: query,
      operationName: "createProject",
      variables: {
        input: {
          name: project.name || "",
          pattern: project.pattern,
          id: project.id,
          plan: project.plan
        }
      }
    }),
    method: "POST",
    headers: { "Authorization": "Bearer " + loginState.jwt }
  });
  const result = await (await remoteReq).json();
  if (result.errors) {
    throw result.errors;
  }
  await pushProgress(project.id, project.localId);
};

const pushProgress = async (id: uuid, localId: string) => {
  const loginState = login.get();
  const lastProgress = syncState[localId] || 0;
  const progress: Array<Progress> = await readTx([Store.Progress], tx => {
    const progress = tx.objectStore(Store.Progress);
    return readAll(progress.openCursor(IDBKeyRange.bound([localId, lastProgress], [localId, Number.MAX_VALUE], true)));
  });
  if (progress.length == 0) {
    return;
  }
  const maxTimestamp = progress.reduce((acc: Progress, cur: Progress) => {
    if (cur.date > acc.date) {
      return cur;
    } else {
      return acc;
    }
  });
  if (!maxTimestamp) {
    return;
  }
  const query = `
mutation updateProject($id: ID!, $input: [UpdateProjectInput!]!) {
  updateProject(id: $id, input: $input)
}`;
  const remoteReq = fetch(endPoint, {
    body: JSON.stringify({
      query: query,
      operationName: "updateProject",
      variables: {
        id: id,
        input: progress.map(p => {return { timestamp: p.date, value: p.value };})
      }
    }),
    method: "POST"
  });
  const result = await (await remoteReq).json();
  if (result.errors) {
    const lastProgress = ((result.errors[0] || {}).extensions || {}).lastProgress;
    if (lastProgress) {
      syncState[localId] = lastProgress;
    }
    if (result.errors[0].message != "progress rejected") {
      throw result.errors;
    }
  } else {
    syncState[localId] = result.data.updateProject;
  }
};

const downloadProject = async (id: string) => {
  const loginState = login.get();
  const query = `
query getProject($id: ID!) {
  project(id: $id) {
    pattern {
        width
        height
        data
        colors
    }
    lastUpdate
    progress {
        nodes {
            timestamp
            value
        }
    }
  }
}`;
  const remoteReq = fetch(endPoint, {
    body: JSON.stringify({
      query: query,
      operationName: "getProject",
      variables: {
        id: id
      }
    }),
    method: "POST",
    headers: { "Authorization": "Bearer " + loginState.jwt }
  });
  const result = await (await remoteReq).json();
  if (result.errors) {
    throw result.errors;
  }
  const project = result.data.project;
  const progress = project.progress.nodes;
  const lastUpdate = project.lastUpdate;
  delete project.progress;
  delete project.lastUpdate;
  project.localId = project.id = id;
  project.user = id;
  await writeTx([Store.Projects], tx => {
    return tx.objectStore(Store.Projects).put(project);
  });
  try {
    await writeTx([Store.Progress], tx => {
      const store = tx.objectStore(Store.Progress);
      const promisses = progress.map(p => {
        const foo: Progress = { project: project.localId, date: p.timestamp, value: p.value };
        return store.put(foo);
      });
      return Promise.all(promisses);
    });
    syncState[project.localId] = lastUpdate;
  } catch (e) {
    Sentry.captureException(e);
  }
};