'use strict';


import * as React from "react";
import { Redirect } from "react-router";
import Button from "@material-ui/core/Button";
import CheckIcon from "@material-ui/icons/Check";
import FolderOpenIcon from "@material-ui/icons/FolderOpen";
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";

import FabGroup from "./FabGroup";
import UUID from "./uuid";
import { find_pattern } from "pattern_importer";

import { ProjectSettings } from "./ProjectSettings";

import { Project, Pattern } from "./types";
import { RouterHistory } from "react-router-dom";

type Props = {
  history: RouterHistory;
};

type StateInner = {type: "FileLoaded";content: string;name: string;} | {type: "PatternFound";project: Project;} | {type: "Done";pattern: Pattern;name: string;} | {type: "Error";error: any | null | undefined;};

type State = {inner: StateInner | null;};


function OpenImage(props: {transition: (arg0: StateInner) => void;}): React.ReactElement<any> {
  const input = React.useRef<HTMLInputElement | null | undefined>(null);
  const onChange = React.useCallback(function onChange(e) {
    var fileToLoad = e.target.files[0];
    var fileReader = new FileReader();
    fileReader.onload = function (e) {
      props.transition({ type: "FileLoaded", content: (e.target.result as string), name: fileToLoad.name });
    };
    fileReader.readAsDataURL(fileToLoad);
  }, []);
  const onClick = React.useCallback(function onClick() {
    if (input.current) {
      input.current.click();
    }
  }, [input]);
  return <Grid container justify="center" alignItems="center" style={{ height: "100vh" }}>
    <Grid item xs={4} sm={4}>
            <Button aria-label="Open" onClick={onClick}>
                <FolderOpenIcon />
                Load pattern from file
            </Button>
            <input type="file" onChange={onChange} ref={input} style={{ visibility: "hidden" }} />
    </Grid>
  </Grid>;
}

function ShowImage(props: {transition: (arg0: StateInner) => void;content: string;name: string;}): React.ReactElement<any> {
  const canvas = React.useRef<HTMLCanvasElement | null | undefined>(null);
  const img = React.useRef<HTMLImageElement | null | undefined>(null);
  const imgLoaded = React.useCallback(() => {
    const c = canvas.current;
    const i = img.current;
    try {
      if (c == null) {
        throw "CI1: canvas is null";
      }
      if (i == null) {
        throw "CI2: img is null";
      }
      const c2d = c.getContext('2d');
      c.width = i.width;
      c.height = i.height;
      if (i.height == 0) {
        throw "CI3: height is 0";
      }
      if (i.width == 0) {
        throw "CI4: width is 0";
      }
      c2d.drawImage(i, 0, 0);
      const pattern = find_pattern(i.width, c2d.getImageData(0, 0, i.width, i.height).data);
      props.transition({ type: "PatternFound", project: { localId: UUID(), pattern: pattern, name: props.name } });
    } catch (e) {
      props.transition({ type: "Error", error: e });
    }
  }, [canvas, img, props.transition, props.name]);
  const onError = React.useCallback(ev => {
    props.transition({ type: "Error", error: ev });
  }, [props.transition]);
  return <div style={{ display: "none" }}>
        <canvas ref={canvas}></canvas>
        <img ref={img} src={props.content} onLoad={imgLoaded} onError={onError} />
    </div>;
}

export default function (props: Props) {
  const [inner, setState] = React.useState(null);
  if (!inner) {
    return <OpenImage transition={setState} />;
  } else if (inner.type === "FileLoaded") {
    return <ShowImage transition={setState} content={inner.content} name={inner.name} />;
  } else if (inner.type === "PatternFound") {
    return <ProjectSettings project={inner.project} progress={0} history={props.history} />;
  } else {
    throw inner.error;
  }
  throw "Boom!";
}
