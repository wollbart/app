'use strict';


import { useRef, useEffect, useState } from "react";
import Hammer from "@egjs/hammerjs";

export default function useHammer(opts) {
  const ref = useRef();
  useEffect(() => {
    const hammer = new Hammer(ref.current, { recognizers: [[Hammer.Pinch, { enable: true }], [Hammer.Tap]] });
    opts.call(null, hammer);
    return () => {
      hammer.stop();
      hammer.destroy();
    };
  });
  return ref;
};