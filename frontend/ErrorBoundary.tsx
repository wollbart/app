"use strict";


import * as React from "react";
import * as Sentry from "@sentry/browser";

export default class ErrorBoundary extends React.Component<{children: React.ReactElement<any>;}, {error?: any;}> {

  constructor(props: {children: React.ReactElement<any>;}) {
    super(props);
    this.state = { error: null };
  }

  componentDidCatch(error: any, errorInfo: any) {
    this.setState({ error });
    Sentry.withScope(scope => {
      Object.keys(errorInfo).forEach(key => {
        scope.setExtra(key, errorInfo[key]);
      });
      Sentry.captureException(error);
    });
  }

  render(): React.ReactElement<any> {
    if (this.state.error) {
      //render fallback UI
      return <div><a onClick={() => Sentry.showReportDialog()}>Report feedback</a>
              <span>Error: {JSON.stringify(this.state.error)}</span></div>;
    } else {
      //when there's not an error, render children untouched
      return this.props.children;
    }
  }
}