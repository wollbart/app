"use strict";


import * as React from "react";
import { makeStyles } from "@material-ui/styles";
export type Props = {
  children: (React.ReactElement<any> | null | undefined) | Array<React.ReactElement<any>>;
};

const useStyle = makeStyles({
  fab: {
    display: "flex",
    flexDirection: "column",
    position: "fixed",
    bottom: "15px",
    right: "15px",
    padding: "15px",
    alignItems: "center",
    "& > *": {
      margin: "5px"
    }
  }
});

export default function FabGroup(props: Props) {
  const styles = useStyle();
  return <div className={styles.fab}>{props.children}</div>;
}