import React from "react";
import Db from "./Db";
import indexedDB from "fake-indexeddb";
import TestRenderer from "react-test-renderer";


window.indexedDB = indexedDB;

afterEach(() => {
  indexedDB.deleteDatabase("wollbart");
});

test("it opens a database", () => {
  let rendered = null;
  return new Promise((resolve, reject) => {
    TestRenderer.act(() => {
      rendered = TestRenderer.create(<Db.Open>
                    <Db.Consumer>{db => {resolve(db);return <p>Hello</p>;}}</Db.Consumer>
                </Db.Open>);
    });
  }).then(db => {
    expect(db).not.toBeNull();
    expect(db.name).toBe("wollbart");
    expect(db.version).toBe(1);
    expect(rendered).toMatchSnapshot();
    TestRenderer.act(() => {
      rendered.unmount();
    });
  });
});