'use strict';

import * as React from "react";
import * as Sentry from "@sentry/browser";
import { Project, Progress, StartLogin, Observable } from "./types";
import { getContext } from 'svelte';

const Context = React.createContext<Db2 | null | undefined>(null);

export const Type = {
  Progress: "Progress",
  GetProjectAndProgress: "GetProjectAndProgress",
  GetProjects: "GetProjects",
  GetProgressAndStreak: "GetProgressAndStreak",
  SaveProject: "SaveProject",
  Login: "Login",
  Logout: "Logout",
  CurrentUser: "CurrentUser"
};

export type Message = {method: typeof Type.Progress;progress: Progress;} | {method: typeof Type.GetProjects;} | {method: typeof Type.GetProgressAndStreak;id: string;} | {method: typeof Type.GetProjectAndProgress;id: string;} | {method: typeof Type.SaveProject;project: Project;} | {method: typeof Type.Login;jwt: string;name: string;startLogin: StartLogin;} | {method: typeof Type.Logout;} | {method: typeof Type.CurrentUser;};
export type Result<T> = {loading: true;} | {result: T;} | {error: any;};

export type CurrentUser = {id: uuid | null | undefined;name: string | null | undefined;};

function observable<T>(p: Promise<T>): Observable<T> {
	return {
		subscribe: (fn) => {
			p.then(fn);
			return () => {}
		}
	}
}

class Db2 {

  channel: MessageChannel;
  constructor() {
    this.channel = new MessageChannel();
  }
  progress(progress: Progress) {
    this.channel.port1.postMessage(({ method: Type.Progress, progress: progress } as Message));
  }
  getProjectAndProgress(id: string): Result<[Project | null | undefined, Progress | null | undefined]> {
    return this.send({ method: Type.GetProjectAndProgress, id: id });
  }
  getProjects(): Result<Array<Project>> {
    return this.send({ method: Type.GetProjects });
  }
  getProgressAndStreak(id: string): Result<[{value: number;}, {value: number;}]> {
    return this.send({ method: Type.GetProgressAndStreak, id: id });
  }
  saveProject(project: Project): Result<boolean> {
    return this.send({ method: Type.SaveProject, project: project });
  }
  login(jwt: string, name: string, startLogin: StartLogin): Result<boolean> {
    return this.send({ method: Type.Login, jwt: jwt, name: name, startLogin: startLogin });
  }
  logout(): Result<boolean> {
    return this.send({ method: Type.Logout });
  }
  currentUser(): Result<CurrentUser> {
    return this.send({ method: Type.CurrentUser });
  }
  send(message: Message): Result<any> {
    const [state, setState] = React.useState<Result<any>>({ loading: true });
    React.useEffect(() => {
      const chan = new MessageChannel();
      this.channel.port1.postMessage(message, [chan.port2]);
      chan.port1.onmessage = ev => {
        setState(ev.data);
      };
      chan.port1.onerror = ev => {
        setState({ error: ev.error });
      };
      return () => {
        chan.port1.postMessage(null);
        chan.port1.close();
      };
    }, []);
    return state;
  }
  ssend(message: Message): Observable<any> {
     const chan = new MessageChannel();
     this.channel.port1.postMessage(message, [chan.port2]);
     return observable(new Promise((resolve, reject) =>  {
     	chan.port1.onmessage = ev => {
          resolve(ev.data);
        chan.port1.close();
      	};
        chan.port1.onerror = ev => {
          reject(ev.error);
        chan.port1.close();
        };
        // chan.port1.postMessage(null);
      }));
  }
}

function newDb(): Db2 {
  return new Db2();
}

function lazyRef<T>(init: () => T): T {
  const ref = React.createRef();
  if (ref.current == null) {
    return ref.current = init();
  } else {
    return ref.current;
  }
}

export function useDb(): Db2 {
  const ctx = React.useContext(Context);
  if (!ctx) {
    throw "Db2 not opened";
  }
  return ctx;
}

export const key = {};

export function getDb(): Db2 {
	const db = getContext(key);
	if( !db ) {
		throw "Db2 not openend";
	}
	return db;
}


export function Open(props: {children: React.ReactNode;}) {
  const {
    children
  } = props;
  const db = lazyRef(newDb);
  React.useEffect(() => {
    const worker = new Worker("./dbWorker.ts");
    worker.onerror = Sentry.captureException;
    worker.postMessage("start", [db.channel.port2]);
    return () => {worker.terminate();};
  }, []);
  return <Context.Provider value={db}>{children}</Context.Provider>;
}

export default {
  Open: Open,
  useDb: useDb,
  getDb: getDb,
  key: key
};
