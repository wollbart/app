#[macro_use]
extern crate serde_derive;

use wasm_bindgen::prelude::*;
use wasm_bindgen::Clamped;
use stats::Frequencies;
use std::borrow::Cow;

#[derive(Serialize)]
pub struct Pattern {
    pub width: usize,
    pub height: usize,
    pub colors: Vec<String>,
    pub data: Vec<u8>
}

#[derive(Serialize,Eq,PartialEq,Debug)]
pub struct Point {
    pub x: usize,
    pub y: usize
}

impl Pattern {

    pub fn from_image(image: &Image) -> Self {
        let mut colors : Vec<Pixel> = Vec::with_capacity(16);
        let data = image.data.iter().map(|pixel: &Pixel|{
            match colors.iter().position(|other| other == pixel) {
                Some(idx) => {
                    idx as u8
                },
                None => {
                    colors.push(*pixel);
                    (colors.len() - 1) as u8
                }
            }
        }).collect();
        // TODO: check that |colors| < 256
        Pattern{
            width: image.width,
            height: image.height,
            colors: colors.iter().map(|color| color.to_string() ).collect(),
            data
        }
    }
}

#[wasm_bindgen]
pub fn find_pattern(width: usize, image_data: Clamped<Vec<u8>>) -> JsValue {
    find_pattern_2d(Image::from(width, &*image_data).expect("invalid image size"), &NoOpReporter())
        .as_ref().map(Pattern::from_image)
        .as_ref().map(|pattern| JsValue::from_serde(pattern).unwrap())
        .unwrap_or(JsValue::NULL)
}

#[derive(Debug,Hash,PartialEq,Eq,Copy,Clone)]
#[repr(transparent)]
pub(crate) struct Pixel{
    data: [u8; 4]
}

impl std::fmt::Display for Pixel {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write![f, "#{:02X}{:02X}{:02X}{:02X}", self.data[0], self.data[1], self.data[2], self.data[3]]
    }
}

impl Pixel {

    fn rgb(r: u8, g: u8, b: u8) -> Pixel {
        Pixel{ data: [r,g,b,255] }
    }

    #[inline]
    fn r(&self) -> u8 {
        self.data[0]
    }

    #[inline]
    fn g(&self) -> u8 {
        self.data[1]
    }

    #[inline]
    fn b(&self) -> u8 {
        self.data[2]
    }

    fn diff(&self, other: &Pixel) -> f32 {
        (
            ( self.r() as f32 - other.r() as f32 ).powi(2) +
            ( self.g() as f32 - other.g() as f32 ).powi(2) +
            ( self.b() as f32 - other.b() as f32 ).powi(2)
        ).sqrt()
    }

    fn value(&self) -> u32 {
        self.r() as u32 + self.g() as u32 + self.b() as u32
    }

    fn avg(&self, other: &Pixel) -> Pixel {
        if self.value() > other.value() {
            return *self
        } else {
            return *other
        }
    }

}

#[derive(Eq,PartialEq,Debug,Clone)]
pub(crate) struct StepBySlice<'a,T: 'a> {
    data: &'a [T],
    step: usize,
    current: usize
}

impl<'a,T: 'a> Iterator for StepBySlice<'a,T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current >= self.data.len() {
            None
        } else {
            let elem = &self.data[self.current];
            self.current = self.current + self.step;
            return Some(elem)
        }
    }
}

pub struct Image<'a> {
    width: usize,
    height: usize,
    data: Cow<'a,[Pixel]>
}

impl<'a> Image<'a> {
    fn from(width: usize, data: &'a [u8]) -> Result<Self,()> {
        if data.len() % ( width * 4 ) != 0 {
            Err(())
        } else {
            Ok(Image{
                width: width,
                height: data.len() / 4 / width,
                data: Cow::Borrowed( unsafe{ std::slice::from_raw_parts(data.as_ptr() as *const Pixel, data.len() / 4) } )
            })
        }
    }

    fn from_pixel_vec(width: usize, data: Vec<Pixel>) -> Result<Self,()> {
        if data.len() % width != 0 {
            Err(())
        } else {
            Ok(Image{
                width: width,
                height: data.len() / width,
                data: Cow::Owned( data )
            })
        }
    }
/*
    fn row(&self, n: usize) -> Option<std::slice::Iter<Pixel>> {
        if n >= self.height {
            return None
        }
        return Some(self.data[(self.width * n)..(self.width * (n+1))].iter())
    }
*/
    fn rows(&self) -> impl Iterator<Item=std::slice::Iter<Pixel>> {
        self.data.chunks(self.width).map(|row|{ row.iter() })
    }

    fn cols(&'a self) -> impl Iterator<Item=StepBySlice<'a,Pixel>> {
        (0..self.width).map(move |i| StepBySlice{ data: &self.data[i..], step: self.width, current: 0 } )
    }

    fn at(&'a self, x: usize, y: usize) -> Pixel {
        self.data[y * self.width + x]
    }

}


#[cfg(not(target_arch="wasm"))]
impl<'a> Image<'a> {

    pub fn read(path: &str) -> Self {
        use std::fs::File;
        let decoder = png::Decoder::new(File::open(path).unwrap());
        let (info, mut reader) = decoder.read_info().unwrap();
        if info.color_type == png::ColorType::RGBA {
            let mut buf = vec![0; info.buffer_size()];
            reader.next_frame(&mut buf).unwrap();
            let p = buf.as_mut_ptr();
            let len = buf.len();
            let cap = buf.capacity();
            let data = unsafe {
                std::mem::forget(buf);
                Vec::from_raw_parts(p as *mut Pixel, len / 4, cap / 4)
            };
            Image{
                width: info.width as usize,
                height: info.height as usize,
                data: Cow::Owned(data)
            }
        } else if info.color_type == png::ColorType::RGB {
            let mut buf = vec![0; info.buffer_size()];
            reader.next_frame(&mut buf).unwrap();
            let data = buf.chunks(3).map(|sl| Pixel::rgb(sl[0],sl[1],sl[2]) ).collect();
            Image{
                width: info.width as usize,
                height: info.height as usize,
                data: Cow::Owned(data)
            }
        } else {
            panic!["Wrong color type: {:?}", info.color_type];
        }
    }

    pub fn write(&self, path: &str) {
        use std::path::Path;
        use std::fs::File;
        use std::io::BufWriter;
        let path = Path::new(path);
        let file = File::create(path).unwrap();
        let ref mut w = BufWriter::new(file);

        let mut encoder = png::Encoder::new(w, self.width as u32, self.height as u32); // Width is 2 pixels and height is 1.
        encoder.set_color(png::ColorType::RGBA);
        encoder.set_depth(png::BitDepth::Eight);
        let mut writer = encoder.write_header().unwrap();
        
        writer.write_image_data(unsafe{ std::slice::from_raw_parts(self.data.as_ptr() as *const u8,self.data.len() * 4) }).unwrap(); // Save
    }
}

trait RangeContains<T: PartialOrd> {
    fn embodies(&self, item: &T) -> bool;
}

impl<T: PartialOrd> RangeContains<T> for std::ops::Range<T> {
    #[inline]
    fn embodies(&self, item: &T) -> bool {
        (&self.start <= item) && (item < &self.end)
    }
}

#[derive(Debug,PartialEq,Eq)]
struct Pattern1d {
    start: usize,
    period: usize,
    end: usize
}

pub(crate) fn find_spikes_1d<'a,I: Iterator<Item=&'a Pixel>>(iterator: I) -> Vec<usize> {
    struct Top {
        diff: f32,
        pos: usize
    }
    enum State<'a> {
        Plane{ base: &'a Pixel },
        Edge{ base: &'a Pixel, top: Top, searched: usize }
    }

    let mut enumerator = iterator.enumerate();
    let mut spikes = vec![];
    // Find edges
    match enumerator.next() {
        None => return spikes,
        Some( ( _, first) ) => {
            let mut current = State::Plane{ base: first };
            for ( pos, nxt ) in enumerator {
                current = match current {
                    State::Plane{ base } => {
                        let diff = base.diff( nxt );
                        if diff > 100.0 {
                            State::Edge{ base, top: Top{ diff, pos }, searched: 0 }
                        } else {
                            State::Plane{ base: nxt }
                        }
                    },
                    State::Edge{ base, top, searched } => {
                        let diff = base.diff( nxt );
                        if diff > top.diff {
                            State::Edge{ base, top: Top{ diff, pos }, searched: searched + 1 }
                        } else if diff > 30.0 {
                            State::Edge{ base, top, searched: searched + 1 }
                        } else {
                            spikes.push( top.pos );
                            State::Plane{ base: nxt }
                        }
                    }
                }
            }
        }
    }
    return spikes;
}

// (offset, size, confidence)
pub(crate) fn find_pattern_1d<'a,I: Iterator<Item=&'a Pixel>>(iterator: I) -> Option<Pattern1d> {
    let spikes = find_spikes_1d(iterator);
    // Find the most common edge distance
    let freq = spikes.windows(2).map(|window| window[1] - window[0]).collect::<Frequencies<_>>();
    match freq.most_frequent().first() {
        None => return None,
        Some( (&diff, occurences) ) => {
            if *occurences < (freq.len() as u64 / 2) {
                return None
            }
            let lower_bound = if diff < 3 { 0 } else { diff - 3 };
            let neighborhood = lower_bound..diff+3;
            let start = spikes.windows(2).find(|window| neighborhood.embodies( &(window[1] - window[0]) ) ).map(|window| window[0] )?;
            let end = spikes.windows(2).rev().find(|window| neighborhood.embodies( &(window[1] - window[0]) ) ).map(|window| window[1] )?;
            return Some( Pattern1d{ start, end, period: diff } )
        }
    }
}


pub trait Reporter {
    fn found_box(&self, origin: Point, width: usize, height: usize);
}

struct NoOpReporter ();

impl Reporter for NoOpReporter {
    fn found_box(&self, _origin: Point, _width: usize, _height: usize) {}
}

pub fn find_pattern_2d<'a,'b,'c>(image: Image<'a>, reporter: &'b dyn Reporter) -> Option<Image<'c>> {
    // Pass 1: find a rough pattern
    let px = {
        let patterns = image.rows().step_by(4).flat_map(|row| find_pattern_1d(row) ).collect::<Vec<Pattern1d>>();
        if patterns.is_empty() {
            return None
        }
        let starts = patterns.iter().map(|pattern| pattern.start ).collect::<Frequencies<_>>();
        let ends = patterns.iter().map(|pattern| pattern.end ).collect::<Frequencies<_>>();
        let periods = patterns.iter().map(|pattern| pattern.period ).collect::<Frequencies<_>>();
        Pattern1d{
            start: *starts.most_frequent().iter().take(3).map(|(n,_)| *n ).min().unwrap(),
            end: *ends.most_frequent().iter().take(3).map(|(n,_)| *n ).max().unwrap(),
            period: *periods.most_frequent().first().unwrap().0
        }
    };
    let py = {
        let patterns = image.cols().step_by(4).flat_map(|row| find_pattern_1d(row) ).collect::<Vec<Pattern1d>>();
        if patterns.is_empty() {
            return None
        }
        let starts = patterns.iter().map(|pattern| pattern.start ).collect::<Frequencies<_>>();
        let ends = patterns.iter().map(|pattern| pattern.end ).collect::<Frequencies<_>>();
        let periods = patterns.iter().map(|pattern| pattern.period ).collect::<Frequencies<_>>();
        println!["periods: {:?}", periods];
        Pattern1d{
            start: *starts.most_frequent().iter().take(3).map(|(n,_)| *n ).min().unwrap(),
            end: *ends.most_frequent().iter().take(3).map(|(n,_)| *n ).max().unwrap(),
            period: *periods.most_frequent().first().unwrap().0
        }
    };
    reporter.found_box(Point{x: px.start,y: py.start}, px.end - px.start, py.end - py.start);

    // Pass 2: refine pattern
    // Patterns are often not scaled by whole numbers.
    // As a result the width of a field is within +/- 0.5 of our first estimate.
    let eheight = (py.end - py.start + py.period / 2) / py.period;
    let ewidth = (px.end - px.start + px.period / 2) / px.period;
    fn mean<'a,I>(i:I, period: usize) -> f64 
        where I: Iterator,
              I::Item: Iterator<Item=&'a Pixel>
    {
        let pxpm1 = period - 1;
        let pxpp1 = period + 1;

        let x_stats = stats::merge_all(
            i.step_by(4).map(|row|
                find_spikes_1d(row).windows(2)
                    .map(|window| window[1] - window[0])
                    .filter(|&diff| diff >= pxpm1 && diff <= pxpp1 )
                    .collect::<stats::Frequencies<_>>()
                )
            ).unwrap();
        let x_sum = x_stats.count(&pxpm1) + x_stats.count(&period) + x_stats.count(&pxpp1);
        return pxpm1 as f64 + (x_stats.count(&period) as f64 / x_sum as f64) + 2.0 * (x_stats.count(&pxpp1) as f64 / x_sum as f64);
    }

    let x_mean = mean(image.rows(), px.period);
    let y_mean = mean(image.cols(), py.period);

    let fheight = ((py.end - py.start) as f64 / y_mean).ceil();
    let fwidth = ((px.end - px.start) as f64 / x_mean).ceil();
    let height = fheight as usize;
    let width = fwidth as usize;
    let bx_mean = (px.end - px.start) as f64 / fwidth;
    let by_mean = (py.end - py.start) as f64 / fheight;

    // Pass 3: sample colors
    let mut result: Vec<Pixel> = Vec::with_capacity(width*height);
    let bx = px.start;
    let by = py.start;
    let pxh = px.period / 2;
    let pyh = py.period / 2;
    for y in 0..height {
        for x in 0..width {
            let ox = bx + (x as f64 * bx_mean) as usize;
            let oy = by + (y as f64 * by_mean) as usize;

            result.push( image.at(ox + 2,oy + 2)
                         .avg(&image.at(ox + 4,oy + 4))
                         .avg( &image.at(ox + px.period - 4, oy + py.period - 4) )
                         .avg( &image.at(ox +  px.period - 4,oy + 4))
                         .avg( &image.at(ox + 4, oy + py.period - 4) )
                         )
        }
    }
    Some( reduce_colors( &Image::from_pixel_vec(width, result).unwrap() ) )
}

fn reduce_colors<'a,'b>(image: &Image<'a>) -> Image<'b> {
    use std::cmp::Ordering;
    // trivial color reducers
    let mut result = vec![];
    let mut colors = vec![];
    for pixel in image.data.iter() {
        let best = colors.iter().map(|other: &Pixel| (*other, other.diff(pixel))).filter(|(_,diff)| *diff < 50.0 ).min_by(|(_,a),(_,b)| a.partial_cmp(b).unwrap_or(Ordering::Equal));
        match best {
            None => {
                colors.push(*pixel);
                result.push(*pixel)
            },
            Some((other,_)) => {
                result.push(other)
            }
        }
    }
    return Image::from_pixel_vec(image.width, result).unwrap()
}

#[cfg(test)]
mod test {
    use png;
    use std::fs::File;
    use super::*;

    #[test]
    fn it_finds_a_pattern() {
        let img = Image::read("./data/rarity_full.png");
        let result = find_pattern_2d( img, &NoOpReporter{} );
        assert_eq![result.is_some(), true];
        if let Some(pattern) = result {
            assert_eq![pattern.width, 40];
            assert_eq![pattern.height, 46];
        }
    }
/*
    #[test]
    fn it_reduces_the_number_of_colours() {
        let img = Image::read("./data/rarity2.png");
        let reduced_image = reduce_colors(&img);
        reduced_image.write("./out3.png");
    }
*/
    #[test]
    fn embodies_works_correctly() {
        assert_eq![(4..10).embodies(&3), false];
        assert_eq![(4..10).embodies(&4), true];
        assert_eq![(4..10).embodies(&7), true];
        assert_eq![(4..10).embodies(&9), true];
        assert_eq![(4..10).embodies(&10), false]
    }

    #[test]
    fn it_finds_a_1d_pattern() {
        let data = vec![
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(255,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(255,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(255,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(255,0,0),
            Pixel::rgb(0,0,0),
            Pixel::rgb(0,0,0)
        ];
        let result = find_pattern_1d(data.iter());
        assert_eq![result, Some(Pattern1d{
            start: 3,
            end: 24,
            period: 7
        })]
    }
}
