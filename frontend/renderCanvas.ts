'use strict';


import { Pattern } from "./types";

export default function render(data: {canvas: any;pattern: Pattern;progress?: number;size?: {width: number;height: number;};}) {
  const {
    canvas,
    pattern,
    progress,
    size
  } = data;
  const ctx = canvas.getContext('2d');
  const colors = pattern.colors.map(c => [parseInt(c.substr(1, 2), 16), parseInt(c.substr(3, 2), 16), parseInt(c.substr(5, 2), 16), 255]);
  if (!size) {
    const pixels = new Uint8ClampedArray(pattern.data.length * 4);
    let k = 0;
    for (var i = 0; i < pattern.data.length; i++) {
      const c = colors[pattern.data[i]];
      pixels[k++] = c[0];
      pixels[k++] = c[1];
      pixels[k++] = c[2];
      pixels[k++] = c[3];
    }

    ctx.putImageData(new ImageData(pixels, pattern.width, pattern.height), 0, 0, 0, 0, pattern.width, pattern.height);
  } else {
    const dx = size.width / pattern.width;
    const dy = size.height / pattern.height;
    const s = Math.ceil(Math.max(dx, dy));

    const rows = Math.ceil(size.height / s);
    const cols = Math.ceil(size.width / s);
    const pixels = new Uint8ClampedArray(cols * rows * s * s * 4);
    const starty = Math.floor((pattern.height - rows) / 2);
    const startx = Math.floor((pattern.width - cols) / 2);
    for (let y = 0; y < rows; y++) {
      for (let x = 0; x < cols; x++) {
        const c = colors[pattern.data[x + startx + pattern.width * (y + starty)]];
        for (let sy = 0; sy < s; sy++) {
          for (let sx = 0; sx < s; sx++) {
            const offset = ((y * s + sy) * cols * s + x * s + sx) * 4;
            pixels[offset + 0] = c[0];
            pixels[offset + 1] = c[1];
            pixels[offset + 2] = c[2];
            pixels[offset + 3] = c[3];
          }
        }
      }
    }
    ctx.putImageData(new ImageData(pixels, cols * s, rows * s), 0, 0);
  }
}