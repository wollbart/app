'use strict';


import * as React from "react";

import { makeStyles } from "@material-ui/styles";

import { Link } from "react-router-dom";
import useSize from "./useSize";
import oneColor from "onecolor";


import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Fab from "@material-ui/core/Fab";
import UndoIcon from "@material-ui/icons/Undo";
import DoneIcon from "@material-ui/icons/Done";
import MyLocationIcon from "@material-ui/icons/MyLocation";
import SettingsIcon from "@material-ui/icons/Settings";
import FastForwardIcon from "@material-ui/icons/FastForward";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import useHammer from "./useHammer";
import * as Sentry from "@sentry/browser";

import { useDb } from "./Db2";
import FabGroup from "./FabGroup";
import WithRouter from "./WithRouter";

import { Project, Pattern, uuid } from "./types";

const UP = "UP";
const DOWN = "DOWN";
const DONE = "DONE";

type Position = {x: number;y: number;direction: "UP" | "DOWN";diagonal: number;position: number;};

export function positionOf(progress: number, width: number, height: number): Position {
  const shortEdge = Math.min(width, height);
  const diags = width + height - 1;
  let sum = 0;
  for (let i = 0; i < diags; i++) {
    let diagLen = Math.min(shortEdge, i + 1, diags - i);
    let nextSum = sum + diagLen;
    if (progress < nextSum) {
      // in diag $i
      const left = progress - sum;
      let padX, padY;
      if (i < width) {
        padX = i;
        padY = 0;
      } else {
        padX = width - 1;
        padY = i - padX;
      }
      if (i % 2 == 1) {
        // UP
        return {
          x: padX - left,
          y: height - 1 - (padY + left),
          direction: UP,
          diagonal: i,
          position: left
        };
      } else {
        // DOWN
        return {
          x: padX - diagLen + left + 1,
          y: height - (padY + (diagLen - left)),
          direction: DOWN,
          diagonal: i,
          position: left
        };
      }
    } else {
      sum = nextSum;
    }
  }
  throw "Boom!";
};

function nextChange(pattern: Pattern, position: Position): number {
  const x = position.x;
  const y = position.y;
  const color = pattern.data[pattern.width * y + x];
  if (position.direction == "UP") {
    for (let i = 0;; i++) {
      const cx = x - i;
      const cy = y - i;
      if (cx == 0 || cy == 0) {
        return i + 1;
      }
      if (pattern.data[pattern.width * (cy - 1) + cx - 1] != color) {
        return i + 1;
      }
    }
  } else {
    const lastx = pattern.width - 1;
    const lasty = pattern.height - 1;
    for (let i = 0;; i++) {
      const cx = x + i;
      const cy = y + i;
      if (cx == lastx || cy == lasty) {
        return i + 1;
      }
      if (pattern.data[pattern.width * (cy + 1) + cx + 1] != color) {
        return i + 1;
      }
    }
  }
  throw "BOooooomm!";
}

function SaveProgress(props: {localId: uuid;progress: number;}): React.ReactElement<any> {
  const db = useDb();
  db.progress({
    project: props.localId,
    date: Date.now(),
    value: props.progress
  });
  return <DoneIcon />;
}

const ProjectViewState = (props: {data: Project;progress: number;}) => {
  const classes = useStyles({ colors: props.data.pattern.colors });
  const [state, trackProgress] = React.useReducer((current, n) => {return { progress: current.progress + n, initial: false };}, { progress: props.progress, initial: true });

  return <div className={classes.root}>
        <AppBar style={{ background: 'transparent', boxShadow: 'none' }} color="default">
            <WithRouter>{history => <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu" onClick={ev => history.push("/")}>
                        <ArrowBackIcon />
                    </IconButton>
                    <span style={{ flexGrow: 1 }}>

                    </span>
                    <IconButton onClick={() => history.push("/project/" + props.data.localId + "/settings")}>
                        <SettingsIcon />
                    </IconButton>
                </Toolbar>}
            </WithRouter>
        </AppBar>
            <ProjectViewInner {...props} progress={state.progress} trackProgress={trackProgress} />
            <div className="storeSuccess">
            {state.initial ? null : <SaveProgress localId={props.data.localId} progress={state.progress} />}
            </div>
        </div>;
};

const PixelBack = React.memo(function PixelBack(props: {data: Project;}): React.ReactElement<typeof React.ReactFragment> {
  const pattern = props.data.pattern;
  const pixel = [];
  let pos = 0;
  for (let y = 0; y < pattern.height; y++) {
    for (let x = 0; x < pattern.width; x++) {
      const color = pattern.data[pos++];
      pixel.push(<use key={"" + x + "x" + y} className={"color-" + color} href="#square" x={(x + pattern.height - y) * 100} y={(x + y) * 100} />);
    }
  }
  return <React.Fragment>{pixel}</React.Fragment>;
});

const PixelDiagonalFull = React.memo(function PixelDiagonalFull(props: {data: Project;diagonal: number;}): React.ReactElement<"g"> {
  const diagonal = props.diagonal;
  const width = props.data.pattern.width;
  const height = props.data.pattern.height;
  let x = 0,
      y = 0;
  if (diagonal < height) {
    // Start at the side
    y = height - 1 - diagonal;
  } else {
    // Start at the top
    x = diagonal - height + 1;
  }
  const pixel = [];
  while (y < height && x < width) {
    const color = props.data.pattern.data[width * y + x];
    pixel.push(<use key={"" + x + "x" + y} className={"color-" + color} href="#square" x={(x + height - y) * 100} y={(x + y) * 100} />);
    x++;
    y++;
  }
  return <g>{pixel}</g>;
});

const LineBack = React.memo(function LineBack(props: {data: Project;}): React.ReactElement<any> {
  const pattern = props.data.pattern;
  const left = [];
  const right = [];
  const top = [];
  const bottom = [];
  for (let y = 0; y < pattern.height; y++) {
    left.push(<text key={y} x={(pattern.height - y) * 100} y={y * 100 - 20}>{pattern.height - y}</text>);
    right.push(<text key={y} x={(pattern.width - y) * 100} y={(pattern.width + y) * 100 + 20}>{pattern.height - y}</text>);
  }
  for (let x = 1; x < pattern.width; x++) {
    bottom.push(<text key={x} x={x * 100} y={x * 100 - 20}>{x}</text>);
    top.push(<text key={x} x={x * 100 - 20} y={(x + pattern.height) * 100 + 20}>{x}</text>);
  }
  return <g className="gridNumbers">
        <g className="left">{left}</g>
        <g className="right">{right}</g>
        <g className="top">{top}</g>
        <g className="bottom">{bottom}</g>
    </g>;
});


function PixelFront(props: {data: Project;diagonal: number;position: number;}): React.ReactElement<"g"> {
  const diagonal = props.diagonal;
  const position = props.position;
  const width = props.data.pattern.width;
  const height = props.data.pattern.height;
  const length = Math.min(width, height, diagonal + 1, width + height - 1 - diagonal);

  const pixel = [];
  if (diagonal % 2 == 0) {
    let x = 0,
        y = 0;
    // DOWN
    if (diagonal < height) {
      // Start at the side
      y = height - 1 - diagonal;
    } else {
      // Start at the top
      x = diagonal - height + 1;
    }
    for (let i = 0; i < length; i++) {
      const color = props.data.pattern.data[width * y + x];
      pixel.push(<use key={"" + x + "x" + y} className={"color-" + color + (i <= position ? "" : " future")} href="#square" x={(x + height - y) * 100} y={(x + y) * 100} />);
      x++;
      y++;
    }
  } else {
    let x = width - 1,
        y = height - 1;
    // UP
    if (diagonal < width) {
      // Start at the bottom
      x = diagonal;
    } else {
      // Start at the side
      y = height - (diagonal - width) - 2;
    }
    for (let i = 0; i < length; i++) {
      const color = props.data.pattern.data[width * y + x];
      pixel.push(<use key={"" + x + "x" + y} className={"color-" + color + (i <= position ? "" : " future")} href="#square" x={(x + height - y) * 100} y={(x + y) * 100} />);
      x--;
      y--;
    }
  }
  return <g>{pixel}</g>;
}

function ProjectViewInner(props: {data: Project;classes: Object;progress: number;trackProgress: (arg0: number) => void;}): React.ReactElement<any> {
  const svg = React.useRef();
  const size = useSize();
  const ratio = size.width / size.height;
  const maxProgress = props.data.pattern.height * props.data.pattern.width - 1;
  const position = positionOf(Math.min(props.progress, maxProgress), props.data.pattern.width, props.data.pattern.height);
  const past = [];
  past.push(<PixelFront data={props.data} diagonal={position.diagonal} position={position.position} key={position.diagonal} />);
  for (let d = 0; d < position.diagonal; d++) {
    past.push(<PixelDiagonalFull diagonal={d} data={props.data} key={d} />);
  }

  const countdown = nextChange(props.data.pattern, position);
  const innerY = (props.data.pattern.width + props.data.pattern.height) * 200;
  const innerX = -props.data.pattern.height * 200;
  const currentSquareX = (position.x + props.data.pattern.height - position.y) * 100;
  const currentSquareY = (position.x + position.y) * 100;

  const [zoom, setZoom] = React.useReducer((l, cb) => {
    cb.call(undefined, l);
    if (svg.current) {
      svg.current.style.transform = svg.current.style.transform.replace(/scale\([^)]+\)/, "scale(" + 1.0 / l.current + ")");

    }
    return l;
  }, { current: 1 });
  const hammerRef = useHammer(hammer => {
    hammer.on("tap", () => props.trackProgress(1));
    hammer.on("pinch", ev => setZoom(zoom => zoom.current = ev.scale));
  });
  const handleWheel = React.useCallback(ev => {
    const dy = ev.deltaY;
    setZoom(zoom => zoom.current = Math.max(0.5, zoom.current * (1 + dy / 250)));
    ev.preventDefault = true;
    return false;
  }, [setZoom]);

  let arrow = null;
  if (position.direction == "UP") {
    arrow = <use href="#arrowUp" className="arrow" x={currentSquareX} y={currentSquareY + 50} />;
  } else {
    arrow = <use href="#arrowDown" className="arrow" x={currentSquareX} y={currentSquareY + 150} />;
  }
  const hPos = position.direction == "UP" ? 0.666 : 0.333;
  return <div ref={hammerRef} onWheel={handleWheel} style={{ width: "" + size.width + "px", height: "" + size.height + "px", overflow: "hidden" }}>
        <svg ref={svg} className="squares" style={{ transform: "translate(" + size.width / 2 + "px, " + size.height * hPos + "px) scale(" + 1.0 / zoom.current + ") translate(-" + currentSquareX + "px, -" + currentSquareY + "px)", "transformOrigin": "0px 0px", width: "" + innerY + "px", height: "" + innerY + "px" }} viewBox={"0 0 " + innerY + " " + innerY}>
                <defs>
                    <path d="M0,0 L-100,100 L0,200 L100,100 Z" id="square" />
                    <path d="M-25,0 L0,25 L25,0" id="arrowDown" />
                    <path d="M-25,0 L0,-25 L25,0" id="arrowUp" />
        <filter id="f1" x="0" y="0">
      <feGaussianBlur in="SourceGraphic" stdDeviation="7" />
    </filter>
                </defs>
                <svg viewBox={"0 0 " + innerY + " " + innerY} width={innerY} height={innerY} y="0" x="0">
                    <LineBack data={props.data} />
                </svg>
                <svg viewBox={"0 0 " + innerY + " " + innerY} width={innerY} height={innerY} y="0" x="0" filter="url(#f1)" style={{ opacity: "0.4" }}>
                    <PixelBack data={props.data} />
                </svg>
                <svg viewBox={"0 0 " + innerY + " " + innerY} width={innerY} height={innerY} y="0" x="0">
                    {past}
                </svg>
                <g key="hud" className={"hud-" + props.data.pattern.data[position.y * props.data.pattern.width + position.x]}>
                    <use href="#square" className="focus" x={currentSquareX} y={currentSquareY} />
                    <text x={currentSquareX} y={currentSquareY + 120} textAnchor="middle" className="countdown">{countdown}</text>
                    {arrow}
                </g>
            </svg>
        <FabGroup>
            <Fab color="secondary" size="medium" onClick={() => props.trackProgress(-1)} disabled={props.progress == 0}>
                 <UndoIcon />
            </Fab>
            <Fab color="primary" onClick={ev => {ev.preventDefault();props.trackProgress(Math.max(countdown - 1, 1));}} disabled={props.progress >= maxProgress}>
                 <FastForwardIcon />
            </Fab>
        </FabGroup>
    </div>;
};

const useStyles = makeStyles({
  root: data => {
    const root = {
      backgroundImage: "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAMAAAAp4XiDAAAAUVBMVEWFhYWDg4N3d3dtbW17e3t1dXWBgYGHh4d5eXlzc3OLi4ubm5uVlZWPj4+NjY19fX2JiYl/f39ra2uRkZGZmZlpaWmXl5dvb29xcXGTk5NnZ2c8TV1mAAAAG3RSTlNAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEAvEOwtAAAFVklEQVR4XpWWB67c2BUFb3g557T/hRo9/WUMZHlgr4Bg8Z4qQgQJlHI4A8SzFVrapvmTF9O7dmYRFZ60YiBhJRCgh1FYhiLAmdvX0CzTOpNE77ME0Zty/nWWzchDtiqrmQDeuv3powQ5ta2eN0FY0InkqDD73lT9c9lEzwUNqgFHs9VQce3TVClFCQrSTfOiYkVJQBmpbq2L6iZavPnAPcoU0dSw0SUTqz/GtrGuXfbyyBniKykOWQWGqwwMA7QiYAxi+IlPdqo+hYHnUt5ZPfnsHJyNiDtnpJyayNBkF6cWoYGAMY92U2hXHF/C1M8uP/ZtYdiuj26UdAdQQSXQErwSOMzt/XWRWAz5GuSBIkwG1H3FabJ2OsUOUhGC6tK4EMtJO0ttC6IBD3kM0ve0tJwMdSfjZo+EEISaeTr9P3wYrGjXqyC1krcKdhMpxEnt5JetoulscpyzhXN5FRpuPHvbeQaKxFAEB6EN+cYN6xD7RYGpXpNndMmZgM5Dcs3YSNFDHUo2LGfZuukSWyUYirJAdYbF3MfqEKmjM+I2EfhA94iG3L7uKrR+GdWD73ydlIB+6hgref1QTlmgmbM3/LeX5GI1Ux1RWpgxpLuZ2+I+IjzZ8wqE4nilvQdkUdfhzI5QDWy+kw5Wgg2pGpeEVeCCA7b85BO3F9DzxB3cdqvBzWcmzbyMiqhzuYqtHRVG2y4x+KOlnyqla8AoWWpuBoYRxzXrfKuILl6SfiWCbjxoZJUaCBj1CjH7GIaDbc9kqBY3W/Rgjda1iqQcOJu2WW+76pZC9QG7M00dffe9hNnseupFL53r8F7YHSwJWUKP2q+k7RdsxyOB11n0xtOvnW4irMMFNV4H0uqwS5ExsmP9AxbDTc9JwgneAT5vTiUSm1E7BSflSt3bfa1tv8Di3R8n3Af7MNWzs49hmauE2wP+ttrq+AsWpFG2awvsuOqbipWHgtuvuaAE+A1Z/7gC9hesnr+7wqCwG8c5yAg3AL1fm8T9AZtp/bbJGwl1pNrE7RuOX7PeMRUERVaPpEs+yqeoSmuOlokqw49pgomjLeh7icHNlG19yjs6XXOMedYm5xH2YxpV2tc0Ro2jJfxC50ApuxGob7lMsxfTbeUv07TyYxpeLucEH1gNd4IKH2LAg5TdVhlCafZvpskfncCfx8pOhJzd76bJWeYFnFciwcYfubRc12Ip/ppIhA1/mSZ/RxjFDrJC5xifFjJpY2Xl5zXdguFqYyTR1zSp1Y9p+tktDYYSNflcxI0iyO4TPBdlRcpeqjK/piF5bklq77VSEaA+z8qmJTFzIWiitbnzR794USKBUaT0NTEsVjZqLaFVqJoPN9ODG70IPbfBHKK+/q/AWR0tJzYHRULOa4MP+W/HfGadZUbfw177G7j/OGbIs8TahLyynl4X4RinF793Oz+BU0saXtUHrVBFT/DnA3ctNPoGbs4hRIjTok8i+algT1lTHi4SxFvONKNrgQFAq2/gFnWMXgwffgYMJpiKYkmW3tTg3ZQ9Jq+f8XN+A5eeUKHWvJWJ2sgJ1Sop+wwhqFVijqWaJhwtD8MNlSBeWNNWTa5Z5kPZw5+LbVT99wqTdx29lMUH4OIG/D86ruKEauBjvH5xy6um/Sfj7ei6UUVk4AIl3MyD4MSSTOFgSwsH/QJWaQ5as7ZcmgBZkzjjU1UrQ74ci1gWBCSGHtuV1H2mhSnO3Wp/3fEV5a+4wz//6qy8JxjZsmxxy5+4w9CDNJY09T072iKG0EnOS0arEYgXqYnXcYHwjTtUNAcMelOd4xpkoqiTYICWFq0JSiPfPDQdnt+4/wuqcXY47QILbgAAAABJRU5ErkJggg==);",
      backgroundColor: "#ddd",
      "& .storeSuccess": {
        position: "absolute",
        left: "15px",
        bottom: "15px"
      },
      "& .gridNumbers": {
        fontFamily: "arial",
        fontSize: "10pt",
        fill: "#ddd"
      }
    };
    const styles = {
      "& .position": {
        fontFamily: "arial",
        fontSize: "10pt",
        fill: "#ddd",
        textShadow: "0 0 10pt #000"
      },
      "& .countdown": {
        fontFamily: "arial",
        fontSize: "40pt",
        fill: "#fff",
        textShadow: "0 0 30pt #000"
      },
      "& .focus": {
        strokeWidth: "10pt",
        stroke: "darkgoldenrod",
        strokeOpacity: ".5",
        fill: "none"
      },
      "& .arrow": {
        strokeWidth: "10pt",
        stroke: "darkgoldenrod",
        strokeOpacity: ".5",
        fill: "none"
      },
      "& .future": {
        strokeOpacity: ".5",
        strokeDasharray: "2 5"
      }
    };
    data.colors.forEach((color, i) => {
      let fg = oneColor(color.substr(0, 7)).lightness() > 0.8 ? "#000" : "#fff";
      styles["& .color-" + i + ""] = {
        fill: color,
        strokeWidth: "3pt",
        stroke: "#202020"
      };
      styles["& .hud-" + i] = {
        "& .countdown": {
          fill: fg
        }
      };
    });
    root["& .squares"] = styles;
    return root;
  }
});

export default function ProjectView(props: {data: Project;progress: number;}): React.ReactElement<any> {

  return <ProjectViewState {...props} />;
}