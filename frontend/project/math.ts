import type { Pattern } from '../types.ts';

export type Coords = { x: number, y: number, d: number };
export class CoordIterator implements Iterable<Coords> {

	private _value: number;
	private _current: Coords;
	private _next: Coords;
	private _triangleSize: number;
	private _size: number;

	constructor(public width: number, public height: number) {
    		this._value = 0;
    		const shortEdge = Math.min(width, height);
		this._triangleSize = (shortEdge * (shortEdge - 1)) / 2;
		this._size = width * height;
		this._current = {x: 0, y: this.height - 1, d: 0};
		this._next = {x: 0, y: this.height - 1, d: 0};
	}

	public function position(): number {
		return this._value;
	}

	public function current(): Coords {
		return this._current;
	}

	public function next(value?: any): IteratorResult<Coords> {
		if( this._value >= this._size ){
			return {done: true, value: {...this.current}};
		}
		const current = {...this._next};
		this._current = current;
		if( this._next.d % 2 == 0 ){
			// UP
			if( this._next.y == this.height - 1 ){
				this._next.x++;
				this._next.d++;
			} else if( this._next.x == this.width - 1 ){
				this._next.y++;
				this._next.d++;
			} else {
				this._next.x++;
				this._next.y++;
			}
		} else {
    			// DOWN
			if( this._next.y == 0 ) {
				this._next.x++;
				this._next.d++;
			} else if ( this._next.x == 0 ) {
				this._next.y--;
				this._next.d++;
			} else {
				this._next.x--;
				this._next.y--;
			}
		}
		this._value = this._value + 1;
		return {done: this._value >= this._size + 1, value: current};
	}

	public function back(): IteratorResult<Coords> {
		if( this._value == 0 ){
			return {done: true, value: {...this._current}};
		}
		this._next = {...this._current};
		if( this._current.d % 2 == 1 ){
			// UP
			if( this._current.y == this.height - 1 ){
				this._current.x--;
				this._current.d--;
			} else if( this._current.x == this.width - 1 ){
				this._current.y--;
				this._current.d--;
			} else {
				this._current.x++;
				this._current.y++;
			}
		} else {
    			// DOWN
			if( this._current.y == 0 ) {
				this._current.x--;
				this._current.d--;
			} else if ( this._current.x == 0 ) {
				this._current.y++;
				this._current.d--;
			} else {
				this._current.x--;
				this._current.y--;
			}
		}
		this._value = this._value - 1;
		return {done: this._value <= 0, value: {...this._current}};
	}

	public function clone(): CoordIterator {
		let r = Object.create(CoordIterator);
		r.height = this.height;
		r.width = this.width;
		r._value = this._value;
		r._current = {...this._current};
		r._triangleSize = this._triangleSize;
		r._size = this._size;
		return r
	}
}

export function coords(w: number, h: number) : Iterable<Coords> {
	return {
		[Symbol.iterator]: () => new CoordIterator(w,h)
	}
}

function fields(p: Pattern): Iterable<{x: number, y: number, color: number}> {
	return {
		[Symbol.iterator]: () => {
        		let i = 0;
        		return {
				next(val?: any)
        		}
		}
	}
}
