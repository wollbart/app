'use strict';
import { $Values } from "utility-types";


import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";

export const Store = {
  Projects: "projects",
  Progress: "progress",
  Users: "users"
};

export type StoreName = $Values<typeof Store>;

type IDBUpgradeNeededEvent = {
  oldVersion: number;
  target: {
    result: IDBDatabase;
  };
};

export const NAME = "wollbart";
export const VERSION = 2;
export const upgrade = (ev: IDBUpgradeNeededEvent) => {
  const db = ev.target.result;
  if (ev.oldVersion < 1) {
    db.createObjectStore(Store.Projects, { keyPath: "localId" });
    db.createObjectStore(Store.Progress, { keyPath: ["project", "date"] });
  }
  if (ev.oldVersion < 2) {
    db.createObjectStore(Store.Users, { keyPath: "id" });
  }
};

function LoadingFull(props: {}): React.ReactElement<any> {
  return <Grid container justify="center" alignItems="center" style={{ height: "100vh" }}>
        <Grid item>
            <CircularProgress />
        </Grid>
    </Grid>;
}

const BlackCircularProgress = withStyles({
  colorPrimary: {
    color: "#000"
  }
})(CircularProgress);

function LoadingIcon(props: {}): React.ReactElement<any> {
  return <BlackCircularProgress size={20} />;
}

function assertNonNull(db: IDBDatabase | null | undefined): IDBDatabase {
  if (db == null) {
    throw "Code DB0: no DB passed in";
  }
  return db;
}

export function promisify<T>(req: IDBRequest | Promise<T> | Array<Promise<T>> | Array<IDBRequest>): Promise<T> {
  if (req instanceof Array) {
    return Promise.all(req.map(promisify));
  } else if ("then" in req) {
    return (req as Promise<T>);
  } else {
    const ireq = (req as IDBRequest);
    return new Promise((resolve, reject) => {
      ireq.onsuccess = ev => resolve(ev.target.result);
      ireq.onerror = ev => reject(ev.target.error);
    });
  }
}

export default {
  Store: Store,
  LoadingFull: LoadingFull,
  LoadingIcon: LoadingIcon,
  promisify: promisify
};