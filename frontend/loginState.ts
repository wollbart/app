'use strict';
import { $ReadOnly } from "utility-types";

import { uuid, Project, Progress, Change, User } from "./types";
export type LoginState = {id: uuid | null | undefined;jwt: string | null | undefined;exp: Date | null | undefined;name: string | null | undefined;};
const initialState = {
  id: null,
  jwt: null,
  exp: null,
  name: null
};
let loginState: LoginState = {
  id: null,
  jwt: null,
  exp: null,
  name: null
};
const listeners = [];
export function get(): $ReadOnly<LoginState> {
  return loginState;
}
export function set(nextLoginState: LoginState) {
  loginState = nextLoginState;
  listeners.forEach(l => l.call(null, loginState));
}
export function listen(ls: (arg0: $ReadOnly<LoginState>) => void) {
  listeners.push(ls);
  ls.call(null, loginState);
}
function reset() {
  set(initialState);
}
export default { get, set, reset, listen };