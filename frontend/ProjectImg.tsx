'use strict';


import * as React from "react";

import { makeStyles } from "@material-ui/styles";
import { Pattern } from "./types";

import CardMedia from "@material-ui/core/CardMedia";

type Props = {
  pattern: Pattern;
  progress: number;
};

type CanvasElement = {
  transferControlToOffscreen?: () => OffscreenControl;
};

type OffscreenControl = {};


const useStyle = makeStyles({
  root: {
    width: "100%"
  }
});

export default React.memo(function ProjectImg(props: Props) {
  const styles = useStyle();
  const ref = React.createRef<CanvasElement>();
  const ref2 = React.createRef<OffscreenControl>();
  const [size, setSize] = React.useState({});
  React.useLayoutEffect(() => {
    if (!size.height) {
      const cs = window.getComputedStyle(ref.current);
      setSize({ height: parseInt(cs.height), width: parseInt(cs.width) });
    }
  });
  const [worker, setWorker] = React.useState(null);
  React.useEffect(() => {
    if (!size.height) {
      return undefined;
    }
    const worker: any = new Worker("./canvasWorker.ts");
    if (ref.current && ref.current.transferControlToOffscreen) {
      if (!ref2.current) {
        ref2.current = ref.current.transferControlToOffscreen();
      }
      worker.onerror = console.error;
      worker.postMessage({ canvas: ref2.current, pattern: props.pattern, progress: props.progress, size: size }, [ref2.current]);
    }
    return () => {worker.terminate();};
  }, [size]);
  return <CardMedia ref={ref} component="canvas" className={styles.root} src="null" width={size.width} height={size.height} />;
});
