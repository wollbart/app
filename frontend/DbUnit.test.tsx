import React from "react";
import Db from "./Db";
import TestRenderer from "react-test-renderer";

window.indexedDB = jest.mock();

const originalError = console.error;

/*console.error = function(msg) {
    if( typeof msg !== "string" || msg.indexOf("Consider adding an error boundary") === -1 ) {
        originalError(msg);
    }
}*/
test("it opens a database", () => {
  const dbMock = {};
  const eventMock = { target: { result: dbMock } };
  const openRequest = {};
  window.indexedDB.open = jest.fn(() => openRequest);
  let rendered;
  TestRenderer.act(() => {
    rendered = TestRenderer.create(<Db.Open>Hello</Db.Open>);
  });
  expect(rendered).toMatchSnapshot();
  TestRenderer.act(() => {
    openRequest.onsuccess(eventMock);
  });
  expect(rendered).toMatchSnapshot();
});

test("it handles errors", () => {
  let error = null;
  let errorInfo = null;
  class TestErrorBoundary extends React.Component {

    componentDidCatch(e, ei) {
      error = e;
      errorInfo = ei;
    }
    render() {
      if (!this.state || !this.state.errored) {
        return this.props.children;
      }
      return null;
    }
    static getDerivedStateFromError() {
      return { errored: true };
    }
  }
  const errorMock = { _suppressLogging: true };
  const eventMock = { error: errorMock };
  const openRequest = {};
  window.indexedDB.open = jest.fn(() => openRequest);
  let rendered;
  TestRenderer.act(() => {
    rendered = TestRenderer.create(<TestErrorBoundary><Db.Open>Hello</Db.Open></TestErrorBoundary>);
  });
  expect(rendered).toMatchSnapshot();
  TestRenderer.act(() => {
    openRequest.onerror(eventMock);
  });
  expect(error).toBe(errorMock);
  expect(rendered).toMatchSnapshot();
});