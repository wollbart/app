import { positionOf } from "./ProjectView";

test("progres 0", () => {
  expect(positionOf(0, 5, 3)).toEqual({ x: 0, y: 2, direction: "DOWN", diagonal: 0, position: 0 });
});

test("progres 1", () => {
  expect(positionOf(1, 5, 3)).toEqual({ x: 1, y: 2, direction: "UP", diagonal: 1, position: 0 });
});

test("progres 2", () => {
  expect(positionOf(2, 5, 3)).toEqual({ x: 0, y: 1, direction: "UP", diagonal: 1, position: 1 });
});

test("progres 3", () => {
  expect(positionOf(3, 5, 3)).toEqual({ x: 0, y: 0, direction: "DOWN", diagonal: 2, position: 0 });
});

test("progres 4", () => {
  expect(positionOf(4, 5, 3)).toEqual({ x: 1, y: 1, direction: "DOWN", diagonal: 2, position: 1 });
});

test("progres 5", () => {
  expect(positionOf(5, 5, 3)).toEqual({ x: 2, y: 2, direction: "DOWN", diagonal: 2, position: 2 });
});

test("progres 6", () => {
  expect(positionOf(6, 5, 3)).toEqual({ x: 3, y: 2, direction: "UP", diagonal: 3, position: 0 });
});

test("progres 7", () => {
  expect(positionOf(7, 5, 3)).toEqual({ x: 2, y: 1, direction: "UP", diagonal: 3, position: 1 });
});

test("progres 8", () => {
  expect(positionOf(8, 5, 3)).toEqual({ x: 1, y: 0, direction: "UP", diagonal: 3, position: 2 });
});

test("progres 9", () => {
  expect(positionOf(9, 5, 3)).toEqual({ x: 2, y: 0, direction: "DOWN", diagonal: 4, position: 0 });
});

test("progres 10", () => {
  expect(positionOf(10, 5, 3)).toEqual({ x: 3, y: 1, direction: "DOWN", diagonal: 4, position: 1 });
});

test("progres 11", () => {
  expect(positionOf(11, 5, 3)).toEqual({ x: 4, y: 2, direction: "DOWN", diagonal: 4, position: 2 });
});

test("progres 12", () => {
  expect(positionOf(12, 5, 3)).toEqual({ x: 4, y: 1, direction: "UP", diagonal: 5, position: 0 });
});

test("progres 13", () => {
  expect(positionOf(13, 5, 3)).toEqual({ x: 3, y: 0, direction: "UP", diagonal: 5, position: 1 });
});

test("progres 14", () => {
  expect(positionOf(14, 5, 3)).toEqual({ x: 4, y: 0, direction: "DOWN", diagonal: 6, position: 0 });
});