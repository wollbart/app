use core::future::Future;
use rust_embed::RustEmbed;
use shared::prelude::*;
use std::task::Poll;

#[derive(RustEmbed)]
#[folder = "frontend/dist/"]
struct Asset;

fn with_cors<T>(
    mut t: T,
) -> impl tower_service::Service<
    hyper::Request<hyper::Body>,
    Response = hyper::Response<hyper::Body>,
    Error = std::convert::Infallible,
    Future = impl Future<Output = Result<hyper::Response<hyper::Body>, std::convert::Infallible>>,
>
where
    T: tower_service::Service<
        hyper::Request<hyper::Body>,
        Response = hyper::Response<hyper::Body>,
        Error = std::convert::Infallible,
    >,
{
    hyper::service::service_fn(move |req| {
        let fut = t.call(req);
        async {
            let mut res = fut.await;
            if let Ok(r) = res.as_mut() {
                r.headers_mut().insert(
                    hyper::header::ACCESS_CONTROL_ALLOW_ORIGIN,
                    "*".parse().unwrap(),
                );
                r.headers_mut().insert(
                    hyper::header::ACCESS_CONTROL_ALLOW_HEADERS,
                    "authorization".parse().unwrap(),
                );
                r.headers_mut().insert(
                    hyper::header::ACCESS_CONTROL_ALLOW_METHODS,
                    "get,post".parse().unwrap(),
                );
            }
            res
        }
    })
}

fn with_span<T>(
    mut t: T,
) -> impl tower_service::Service<
    hyper::Request<hyper::Body>,
    Response = hyper::Response<hyper::Body>,
    Error = std::convert::Infallible,
    Future = impl Future<Output = Result<hyper::Response<hyper::Body>, std::convert::Infallible>>,
>
where
    T: tower_service::Service<
        hyper::Request<hyper::Body>,
        Response = hyper::Response<hyper::Body>,
        Error = std::convert::Infallible,
    >,
{
    hyper::service::service_fn(move |req| {
        use tracing_futures::Instrument;
        let span = tracing::info_span!(
            "servlet.request",
            http.method = %req.method(),
            http.path = req.uri().path_and_query().map(|p| p.path()).unwrap_or(""),
            http.status = tracing::field::Empty
        );
        let _enter = span.enter();
        let fut = t.call(req);
        Instrument::in_current_span(async {
            let res = fut.await;
            match &res {
                Ok(r) => {
                    tracing::Span::current().record("http.status", &r.status().as_u16());
                    tracing::info!("http.response")
                }
                Err(e) => tracing::error!("error: {}", e),
            };
            res
        })
    })
}

fn mime(path: &str) -> hyper::header::HeaderValue {
    if let Some(ix) = path.rfind(".") {
        let ext = &path[ix + 1..];
        match ext {
            "js" => hyper::header::HeaderValue::from_static("application/javascript"),
            "css" => hyper::header::HeaderValue::from_static("text/css"),
            "html" => hyper::header::HeaderValue::from_static("text/html"),
            "wasm" => hyper::header::HeaderValue::from_static("application/wasm"),
            _ => hyper::header::HeaderValue::from_static("application/octetstream"),
        }
    } else {
        hyper::header::HeaderValue::from_static("application/octetstream")
    }
}

impl Asset {
    fn get_response(path: &str) -> Option<hyper::Response<hyper::Body>> {
        Self::get(path).map(|file| {
            hyper::Response::builder()
                .status(200)
                .header(hyper::header::CONTENT_TYPE, mime(&path))
                .body(hyper::Body::from(file))
                .unwrap()
        })
    }
}

impl tower_service::Service<hyper::Request<hyper::Body>> for Asset {
    type Response = hyper::Response<hyper::Body>;
    type Error = std::convert::Infallible;
    type Future = std::pin::Pin<
        Box<
            dyn Future<Output = Result<hyper::Response<hyper::Body>, std::convert::Infallible>>
                + Send,
        >,
    >;

    fn poll_ready(&mut self, _cx: &mut std::task::Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: hyper::Request<hyper::Body>) -> Self::Future {
        let path = req.uri().path().to_owned();
        Box::pin(async move {
            Ok::<_, std::convert::Infallible>(
                Self::get_response(&path[1..])
                    .unwrap_or_else(|| Self::get_response("index.html").unwrap()),
            )
        })
    }
}

async fn server() -> () {
    let config = shared::Config::from_file();
    let d = data::Server::new(&config).await;
    let a = auth::Server::new(&config).await;
    let builder = if let Ok(Some(l)) = listenfd::ListenFd::from_env().take_tcp_listener(0) {
        hyper::Server::from_tcp(l).unwrap()
    } else {
        hyper::Server::bind(&([127u8, 0, 0, 1], 12345u16).into())
    };
    builder
        .serve(hyper::service::make_service_fn(
            move |_: &hyper::server::conn::AddrStream| {
                let mut d = d.clone();
                let mut a = a.clone();
                async {
                    Ok::<_, std::convert::Infallible>(with_span(with_cors(
                        hyper::service::service_fn(move |req| {
                            use tower_service::Service;
                            let uri = req.uri().clone();
                            if uri.path().starts_with("/auth/") {
                                a.call(req)
                            } else if uri.path() == "/graphql" {
                                d.call(req)
                            } else {
                                let mut s = Asset {};
                                s.call(req)
                            }
                        }),
                    )))
                }
            },
        ))
        .await
        .unwrap()
}

#[tokio::main]
async fn main() {
    tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(tracing::Level::INFO)
        .init();
    server().await
}
/*
struct MySubscriber {
}

impl tracing::subscriber::Subscriber for MySubscriber {
    fn enabled(&self, _: &tracing_core::metadata::Metadata<'_>) -> bool {
        true
    }
    fn new_span(&self, _: &tracing_core::span::Attributes<'_>) -> tracing_core::span::Id {
        tracing::span::Id::from_u64
    }
    fn record(&self, _: &tracing_core::span::Id, _: &tracing_core::span::Record<'_>) {
        unimplemented!()
    }
    fn record_follows_from(&self, _: &tracing_core::span::Id, _: &tracing_core::span::Id) {
        unimplemented!()
    }
    fn event(&self, _: &tracing_core::event::Event<'_>) {
        unimplemented!()
    }
    fn enter(&self, _: &tracing_core::span::Id) {
        unimplemented!()
    }
    fn exit(&self, _: &tracing_core::span::Id) {
        unimplemented!()
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(tracing::Level::INFO)
        .init();
    tracing::event![tracing::Level::WARN, "foo"];
    // Create the runtime
    let mut rt = tokio::runtime::Builder::new()
        .basic_scheduler()
        .on_thread_start(|| {
            tracing::subscriber::set_global_default(
                tracing_subscriber::FmtSubscriber::builder()
                    .with_max_level(tracing::Level::INFO)
                    .finish(),
            )
            .expect("no");
        })
        .enable_all()
        .build()?;
    rt.block_on(async {
        server().await;
    });
    Ok(())
}*/
